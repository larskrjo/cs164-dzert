To launch our application:
1. Start Xcode.
2. In the menu: "Window"->"Welcome to Xcode" (maybe the popup-window is already visible if you have this behaviour as default)
3. Click on "Connect to a repository"
4. Provide this information in the location field: "https://your_username@bitbucket.org/LarsKrJo/project3.git" where you replace your_username with your own username at bitbucket
5. Clone project and open it by clicking on Open Project after giving the location information.
6. You're ready to run both the application and the unit tests
