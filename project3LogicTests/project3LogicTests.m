//
//  project3LogicTests.m
//  project3LogicTests
//
//  Created by Edouard Godfrey and Lars Kristian Johansen on 5/2/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import "project3LogicTests.h"
#import "Database.h"

@interface project3LogicTests ()

@property (strong, readwrite, nonatomic) Database *database;

@end

@implementation project3LogicTests

@synthesize database=_database;

/**
 * @brief Sets up the test environment.
 */
- (void)setUp
{
    [super setUp];
    self.database = [Database singleton];
}

/**
 * @brief Tear down the test environment.
 */
- (void)tearDown
{
    [super tearDown];
}

/**
 * @brief Test for checking that the number of cocktails are more than 0.
 */
- (void)testNumberOfCocktails
{
    STAssertTrue([[self.database cocktailsName] count] > 0, @"Number of cocktails were larger than 0");
}

/**
 * @brief Test for checking that the number of ingredients are more than 0.
 */
- (void)testNumberOfIngredients
{
    STAssertTrue([[self.database ingredientsName] count] > 0, @"Number of ingredients were larger than 0");
}

@end
