//
//  project3ApplicationTests.m
//  project3ApplicationTests
//
//  Created by Edouard Godfrey and Lars Kristian Johansen on 5/2/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import "project3ApplicationTests.h"
#import "AppDelegate.h"
#import "TabBarController.h"
#import "DetailViewController.h"
#import "BrowseViewController.h"
#import "Database.h"

@interface project3ApplicationTests ()

@property (readwrite, nonatomic, weak) AppDelegate *appDelegate;
@property (readwrite, nonatomic, weak) TabBarController *tabBarController;
@property (readwrite, nonatomic, weak) UIViewController *browseViewController;
@property (readwrite, nonatomic, weak) UIViewController *ingredientViewController;
@property (readwrite, nonatomic, weak) SuggestionViewController *suggestionViewController;

@end


@implementation project3ApplicationTests

@synthesize appDelegate=_appDelegate;
@synthesize tabBarController=_tabBarController;
@synthesize browseViewController=_browseViewController;
@synthesize ingredientViewController=_ingredientViewController;
@synthesize suggestionViewController=_suggestionViewController;

/**
 * @brief Sets up the test environment.
 */
- (void)setUp
{
    [super setUp];
    self.appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.tabBarController = self.appDelegate.tabBarController;
    self.browseViewController = self.appDelegate.browseViewController;
    self.ingredientViewController = self.appDelegate.ingredientsViewController;
    self.suggestionViewController = self.appDelegate.suggestionViewController;
}

/**
 * @brief Tear down the test environment.
 */
- (void)tearDown
{
    [super tearDown];
}

/**
 * @brief Testing that all the viewes are initialized when you launch the application.
 */
- (void) testInit
{
    STAssertNotNil(self.appDelegate, @"AppDelegate does not exist when launching the application");
    STAssertNotNil(self.tabBarController, @"TabBarController does not exist when launching the application");
    STAssertNotNil(self.browseViewController, @"BrowseViewController does not exist when launching the application");
    STAssertNotNil(self.ingredientViewController, @"IngredientViewController does not exist when launching the application");
    STAssertNotNil(self.suggestionViewController, @"SuggestionViewController does not exist when launching the application");
}

/**
 * @brief Test the TabBarControllers initial view
 */
- (void) testInitialView
{
    STAssertTrue(([self.tabBarController selectedViewController] == self.browseViewController), @"Initial view from tab is not the browseview");
}

/**
 * @brief Test switching between controllers in the TabBarController
 */
- (void) testSwitchingView
{
    [self.tabBarController setSelectedViewController:(UIViewController *)self.suggestionViewController];
    STAssertTrue(([self.tabBarController selectedViewController] == (UIViewController *)self.suggestionViewController), @"Switching tabview did not work");
}

/**
 * @brief Test that the detail view rating displays the correct information
 */
- (void) testDetailViewRating
{
    
    DetailViewController *dvController = [[DetailViewController alloc] initWithNibName:@"DetailViewController" bundle:[NSBundle mainBundle]];
    dvController.selectedCocktail = [[[Database singleton] cocktailsName] objectAtIndex:(arc4random()%([[[Database singleton] cocktailsName] count]))];
    dvController.delegate = (BrowseViewController*)self.browseViewController;
    [self.browseViewController.navigationController pushViewController:dvController animated:YES];
    [self.browseViewController presentModalViewController:dvController animated:YES];
    
    int a = [[Database singleton] getCocktailScore:dvController.selectedCocktail];
    
    UIImage *img1 = [UIImage imageNamed:@"suggestion_yellow.png"];
    UIImage *img2 = [UIImage imageNamed:@"suggestion.png"];
    
    if(a == 0) {
        STAssertTrue(img2 == [dvController.star1 backgroundImageForState:UIControlStateNormal], @"Rating was not displayed correctly");
        STAssertTrue(img2 == [dvController.star2 backgroundImageForState:UIControlStateNormal], @"Rating was not displayed correctly");
        STAssertTrue(img2 == [dvController.star3 backgroundImageForState:UIControlStateNormal], @"Rating was not displayed correctly");
        STAssertTrue(img2 == [dvController.star4 backgroundImageForState:UIControlStateNormal], @"Rating was not displayed correctly");
        STAssertTrue(img2 == [dvController.star5 backgroundImageForState:UIControlStateNormal], @"Rating was not displayed correctly");
    }
    else if(a == 1) {
        STAssertTrue(img1 == [dvController.star1 backgroundImageForState:UIControlStateNormal], @"Rating was not displayed correctly");
        STAssertTrue(img2 == [dvController.star2 backgroundImageForState:UIControlStateNormal], @"Rating was not displayed correctly");
        STAssertTrue(img2 == [dvController.star3 backgroundImageForState:UIControlStateNormal], @"Rating was not displayed correctly");
        STAssertTrue(img2 == [dvController.star4 backgroundImageForState:UIControlStateNormal], @"Rating was not displayed correctly");
        STAssertTrue(img2 == [dvController.star5 backgroundImageForState:UIControlStateNormal], @"Rating was not displayed correctly");
    }
    else if(a == 2) {
        STAssertTrue(img1 == [dvController.star1 backgroundImageForState:UIControlStateNormal], @"Rating was not displayed correctly");
        STAssertTrue(img1 == [dvController.star2 backgroundImageForState:UIControlStateNormal], @"Rating was not displayed correctly");
        STAssertTrue(img2 == [dvController.star3 backgroundImageForState:UIControlStateNormal], @"Rating was not displayed correctly");
        STAssertTrue(img2 == [dvController.star4 backgroundImageForState:UIControlStateNormal], @"Rating was not displayed correctly");
        STAssertTrue(img2 == [dvController.star5 backgroundImageForState:UIControlStateNormal], @"Rating was not displayed correctly");
    }
    else if(a == 3) {
        STAssertTrue(img1 == [dvController.star1 backgroundImageForState:UIControlStateNormal], @"Rating was not displayed correctly");
        STAssertTrue(img1 == [dvController.star2 backgroundImageForState:UIControlStateNormal], @"Rating was not displayed correctly");
        STAssertTrue(img1 == [dvController.star3 backgroundImageForState:UIControlStateNormal], @"Rating was not displayed correctly");
        STAssertTrue(img2 == [dvController.star4 backgroundImageForState:UIControlStateNormal], @"Rating was not displayed correctly");
        STAssertTrue(img2 == [dvController.star5 backgroundImageForState:UIControlStateNormal], @"Rating was not displayed correctly");
    }
    else if(a == 4) {
        STAssertTrue(img1 == [dvController.star1 backgroundImageForState:UIControlStateNormal], @"Rating was not displayed correctly");
        STAssertTrue(img1 == [dvController.star2 backgroundImageForState:UIControlStateNormal], @"Rating was not displayed correctly");
        STAssertTrue(img1 == [dvController.star3 backgroundImageForState:UIControlStateNormal], @"Rating was not displayed correctly");
        STAssertTrue(img1 == [dvController.star4 backgroundImageForState:UIControlStateNormal], @"Rating was not displayed correctly");
        STAssertTrue(img2 == [dvController.star5 backgroundImageForState:UIControlStateNormal], @"Rating was not displayed correctly");
    }
    else if(a == 5) {
        STAssertTrue(img1 == [dvController.star1 backgroundImageForState:UIControlStateNormal], @"Rating was not displayed correctly");
        STAssertTrue(img1 == [dvController.star2 backgroundImageForState:UIControlStateNormal], @"Rating was not displayed correctly");
        STAssertTrue(img1 == [dvController.star3 backgroundImageForState:UIControlStateNormal], @"Rating was not displayed correctly");
        STAssertTrue(img1 == [dvController.star4 backgroundImageForState:UIControlStateNormal], @"Rating was not displayed correctly");
        STAssertTrue(img1 == [dvController.star5 backgroundImageForState:UIControlStateNormal], @"Rating was not displayed correctly");
    }
        
}

@end
