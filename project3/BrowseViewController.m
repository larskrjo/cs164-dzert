//
//  BrowseViewController.m
//  project3
//
//  Created by Edouard Godfrey and Lars Kristian Johansen on 4/10/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//


#import "BrowseViewController.h"
#import "CocktailDataSource.h"
#import "Three20/Three20+Additions.h"

@implementation BrowseViewController

@synthesize delegate = _delegate;

///////////////////////////////////////////////////////////////////////////////////////////////////
// NSObject

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        _delegate = nil;

        self.title = @"Browse";
        self.dataSource = [[CocktailDataSource alloc] init];
    }
    return self;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// UIViewController

- (void)loadView {
    [super loadView];
    TTTableViewController* searchController = [[TTTableViewController alloc] init];
    searchController.dataSource = [[CocktailSearchDataSource alloc] init];
    self.searchViewController = searchController;
    self.tableView.tableHeaderView = _searchController.searchBar;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// TTTableViewController

- (void)didSelectObject:(id)object atIndexPath:(NSIndexPath*)indexPath {
    [_delegate searchBrowseViewController:self didSelectObject:object];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// TTSearchTextFieldDelegate

- (void)textField:(TTSearchTextField*)textField didSelectObject:(id)object {
    [_delegate searchBrowseViewController:self didSelectObject:object];
}

@end
