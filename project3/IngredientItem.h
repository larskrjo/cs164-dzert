//
//  IngredientItemCell.h
//  project3
//
//  Created by Edouard GODFREY on 6/16/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import <Three20/Three20.h>
#import "Ingredient.h"

@interface IngredientItem : TTTableTextItem

/*@property (nonatomic, assign) NSInteger uid;
@property (nonatomic, assign) double score;
@property (nonatomic, assign) NSInteger selected;*/
@property (nonatomic, weak) Ingredient *ingredient;

/*
+ (id)itemWithText:(NSString*)text 
               uid:(NSInteger)uid
          selected:(NSInteger)selected;*/
+ (id)itemWithIngredient:(Ingredient *)ingredient;

@end

@interface IngredientItemCell : TTTableTextItemCell

- (void)switchSelection;

@end  
