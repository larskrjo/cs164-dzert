//
//  Tip.m
//  project3
//
//  Created by Edouard GODFREY on 6/8/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import "Tip.h"

@implementation Tip

@synthesize uid=_uid;
@synthesize text=_text;
@synthesize ratingPlus=_ratingPlus;
@synthesize ratingMinus=_ratingMinus;
@synthesize author=_author;
@synthesize date=_date;

+ (id)tipWithId:(NSInteger)_id text:(NSString *)text plus:(NSInteger)plus minus:(NSInteger)minus author:(NSString *)author  date:(double)secs {
    Tip *ret = [[Tip alloc] init];
    if (ret) {
        ret.uid = _id;
        ret.text = text;
        ret.ratingPlus = plus;
        ret.ratingMinus = minus;
        ret.author = author;
        ret.date = [NSDate dateWithTimeIntervalSince1970:secs];
    }
    return ret;
}

@end
