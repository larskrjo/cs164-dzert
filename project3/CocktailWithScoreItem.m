//
//  CocktailWithScoreItem.m
//  project3
//
//  Created by Edouard GODFREY on 6/16/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//
#import <Three20/Three20.h>
#import "CocktailWithScoreItem.h"
#import "Three20/Three20+Additions.h"
#import "GraphicsUtil.h"

@implementation CocktailWithScoreItem
@synthesize score=_score, type=_type;

///////////////////////////////////////////////////////////////////////////////////////////////////  
// class public  
+ (id)itemWithText:(NSString*)text 
          subtitle:(NSString *)subtitle
               URL:(NSString*)url
             score:(double)score
              type:(CocktailWithScoreItemType)type {
    CocktailWithScoreItem *item = [[CocktailWithScoreItem alloc] init];
    if (item) {
        item.text = text;
        item.subtitle = subtitle;
        item.URL = url;
        item.score = score;
        item.type = type;
    }
    return item;  
    
}

///////////////////////////////////////////////////////////////////////////////////////////////////  
// NSObject  

- (id)init {  
    if (self = [super init]) {  
        // ..
    }  
    return self;  
} 

@end

///////////////////////////////////////////////////////////////////////////////////////////////////  

static CGFloat kHPadding = 10; 
static CGFloat kVPadding = 10;
static CGFloat kImageHeight = 20;

///////////////////////////////////////////////////////////////////////////////////////////////////  

//////////////////////////////////////////////////////////////  
//////   CocktailWithScoreItemCell     ////  
//////////////////////////////////////////////////////////////  

@implementation CocktailWithScoreItemCell  

@synthesize score=_score, lblScore=_lblScore;

+ (CGFloat)tableView:(UITableView*)tableView rowHeightForObject:(id)item {  
    return [TTTableSubtitleItemCell tableView:tableView rowHeightForObject:item] + 2*kVPadding + kImageHeight; 
}

///////////////////////////////////////////////////////////////////////////////////////////////////  

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)identifier { 
    if (self = [super initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:identifier]) {  
        _item = nil;
        self.lblScore = [[UILabel alloc] initWithFrame:CGRectZero];
        _lblScore.font = TTSTYLEVAR(tableTimestampFont);
        _lblScore.textColor = TTSTYLEVAR(timestampTextColor);
        [self.contentView addSubview:_lblScore];
        self.score = [[UIImageView alloc] initWithFrame:CGRectZero];
        [self.contentView addSubview:_score];
    }  
    return self;  
}

///////////////////////////////////////////////////////////////////////////////////////////////////  
// UIView  


- (void)layoutSubviews {
    [super layoutSubviews];
    self.textLabel.top = kVPadding;
    self.subtitleLabel.top = self.textLabel.bottom + kVPadding;
    self.score.top = self.subtitleLabel.bottom + kVPadding;
    self.score.left = self.subtitleLabel.left;
    self.lblScore.right = self.contentView.right - 2*kHPadding;
    self.lblScore.top = self.score.centerY - self.lblScore.height / 2.0;
}  

///////////////////////////////////////////////////////////////////////////////////////////////////  
// TTTableViewCell  

- (id)object {  
    return _item;  
}  

- (void)setObject:(id)object {  
    if (_item != object) {  
        [super setObject:object];  
        self.selectionStyle = TTSTYLEVAR(tableSelectionStyle);
        
        CocktailWithScoreItem* item = object; 
        UIImage *filled;
        UIImage *empty;
        NSInteger number;
        float matchScore;
        switch (item.type) {
            case CocktailWithScoreItemTypeGreenBar:
                filled = [UIImage imageNamed:@"bar_small"];
                empty = [UIImage imageNamed:@"void_bar_small"];
                number = 10;
                matchScore = item.score * number;
                self.lblScore.text = [NSString stringWithFormat:@"[%d%% feasible]",(int)(item.score*100) ];
                break;
            case CocktailWithScoreItemTypeRedStar:
                filled = [UIImage imageNamed:@"red_star"];
                empty = [UIImage imageNamed:@"grey_star"];
                number = 5;
                matchScore = item.score;
                self.lblScore.text = [NSString stringWithFormat:@"[%@/%d]",[self formatTwoDecimals:item.score],number ];
                break;
            case CocktailWithScoreItemTypeYellowStar:
                filled = [UIImage imageNamed:@"yellow_star"];
                empty = [UIImage imageNamed:@"grey_star"];
                number = 5;
                matchScore = item.score;
                self.lblScore.text = [NSString stringWithFormat:@"[%@/%d]",[self formatTwoDecimals:item.score],number ];
                break;
            case CocktailWithScoreItemTypeBlueStar:
                filled = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"blue_star" ofType:@"png"]];
                empty = [UIImage imageNamed:@"grey_star"];
                number = 5;
                matchScore = item.score;
                self.lblScore.text = [NSString stringWithFormat:@"[%@/%d]",[self formatTwoDecimals:item.score],number ];
                break;
        }
        UIImage *image = [GraphicsUtil composeBisImageWithImage1:filled andImage2:empty withProportionOfIm1:matchScore andTotalNumber:number];
        self.score.image = image;
        self.score.size = image.size;
        [self.lblScore sizeToFit];
    }  
}

-(NSString *)formatTwoDecimals:(float)input {
    NSString *ret;
    if (trunc(input) == input) {
        ret = [NSString stringWithFormat:@"%d", (int)input];
    } else {
        ret = [NSString stringWithFormat:@"%.2f", input];
    }
    return ret;
}

-(void)prepareForReuse {
    [super prepareForReuse];
    _score.image = nil;
    //_score 
    //[_score removeFromSuperview];
}

@end
