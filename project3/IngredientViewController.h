//
//  IngredientViewController.h
//  project3
//
//  Created by Edouard Godfrey and Lars Kristian Johansen on 4/10/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import <Three20/Three20.h>

@class IngredientDataSource;
@class IngredientViewController;

@protocol IngredientViewControllerDelegate <NSObject>

- (void)searchIngredientViewController:(IngredientViewController*)controller didSelectObject:(id)object;

@end

@interface IngredientViewController : TTTableViewController <UITableViewDelegate,IngredientViewControllerDelegate, UISearchBarDelegate>

@property(nonatomic,weak) id<IngredientViewControllerDelegate> delegate;

@end