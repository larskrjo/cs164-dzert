//
//  SingleCocktailDataSource.h
//  project3
//
//  Created by Edouard GODFREY on 6/6/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import <Three20/Three20.h>
#import "Cocktail.h"
#import "StarRatingItem.h"

/*
 * a searchable model which can be configured with a 
 * loading and/or search time
 */
@interface SingleCocktail : NSObject <TTModel,UIAlertViewDelegate> {
    NSMutableArray* _delegates;
    BOOL _loadingStarted;
    BOOL _loadedDB;
    BOOL _loadedTips;
    BOOL _loadedPhotoPaths;
}


@property(nonatomic,strong) Cocktail *cocktail;
@property(nonatomic,copy) NSArray *ingredients;
@property(nonatomic,copy) NSArray *ingredientsQuantities;
@property(nonatomic,strong) NSMutableArray *tips;
@property(nonatomic,strong) NSMutableArray *photoPaths;
@property(nonatomic,copy) NSString *recipe;
@property(nonatomic,assign) NSInteger score;


- (id)initWithCocktailId:(NSInteger)cid;

@end

@interface SingleCocktailDataSource : TTSectionedDataSource {
    SingleCocktail* _cocktail;
}

@property(nonatomic,strong,readonly) SingleCocktail* cocktail;
@property(nonatomic,weak) id<StarRatingItemDelegate> starDelegate;

- (id)initWithCocktailId:(NSInteger)cid;

@end

@interface SingleCocktailFromFridgeDataSource : SingleCocktailDataSource

@end