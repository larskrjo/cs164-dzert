//
//  AlbumPhotoViewController.m
//  project3
//
//  Created by Edouard GODFREY on 6/14/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import "AlbumPhotoViewController.h"
#import "Constants.h"
#import "ASIFormDataRequest.h"
#import "MBProgressHUD.h"

#define POST_RATING_PHOTO 0

@interface AlbumPhotoViewController ()
- (NSString *)photoIdFromPhoto:(id<TTPhoto>)photo;
- (void)ratingEnabled:(BOOL)b;
@end

@implementation AlbumPhotoViewController
@synthesize cool=_cool,notcool=_notcool;

- (NSString *)photoIdFromPhoto:(id<TTPhoto>)photo {
    NSString *photo_url = [photo URLForVersion:TTPhotoVersionLarge];
    NSRange range = [photo_url rangeOfString:@"/" options:NSBackwardsSearch];
    NSString *photo_id = [photo_url substringFromIndex:range.location+1];
    return photo_id;
}

- (void)ratingEnabled:(BOOL)enabled {
    if (enabled) {
        NSMutableArray *items = [NSMutableArray array]; 
        if (![[_toolbar items] containsObject:_notcool])
            [items addObject:_notcool]; 
        [items addObjectsFromArray:[_toolbar items]];
        if (![[_toolbar items] containsObject:_cool])
            [items addObject:_cool];
        [_toolbar setItems:items];
    } else {
        NSMutableArray *items = [[_toolbar items] mutableCopy];
        [items removeObject:_notcool];
        [items removeObject:_cool];
        [_toolbar setItems:items];
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.cool = [[UIBarButtonItem alloc] initWithTitle:@"Like it" style:UIBarButtonItemStyleBordered target:self action:@selector(action:)]; 
    _cool.tag = 2; // using 1 creates a conflict with other uibutton !
    self.notcool = [[UIBarButtonItem alloc] initWithTitle:@"Dislike" style:UIBarButtonItemStyleBordered target:self action:@selector(action:)]; 
    _notcool.tag = -2;
    
    NSMutableArray *items = [NSMutableArray array]; 
    [items addObject:_notcool]; 
    [items addObjectsFromArray:[_toolbar items]];
    [items addObject:_cool];
    
    [_toolbar setItems:items];
}

- (void)action:(id)sender{ 
    if (! [sender isKindOfClass:[UIBarButtonItem class]])
        return;
    NSInteger dr = ((UIBarButtonItem *)sender).tag / 2;
    // Start request
    NSURL *url = [NSURL URLWithString:[BASE_URL stringByAppendingString:@"photos/post_rating.php"]];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    request.tag = POST_RATING_PHOTO;
    [request setPostValue:[self photoIdFromPhoto:_centerPhoto] forKey:@"photo_id"];
    [request setPostValue:[NSNumber numberWithInt:dr] forKey:@"delta"];
    [request setDelegate:self];
    [request startAsynchronous];
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Sending rating...";
} 

//////////////////////////////////////////////////////////////
// ASI

- (void)requestFinished:(ASIHTTPRequest *)request
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    NSString *responseString = [request responseString];
    NSError *e = nil;
    NSData *data = [responseString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data 
                                                                 options: NSJSONReadingMutableContainers error: &e];
    NSLog(@"Response: %@", responseString);
    NSString *status = [responseDict objectForKey:@"status"];
    if (! [status isEqualToString:@"ok"]) {
        NSLog(@"Error");
        return;
    }
    [self ratingEnabled:NO];
    
    // save id of rated photos
    [[NSUserDefaults standardUserDefaults] setBool:YES 
                                            forKey:[responseDict objectForKey:@"path"] ];
    
}

- (void)didMoveToPhoto:(id<TTPhoto>)photo fromPhoto:(id<TTPhoto>)fromPhoto {
    NSString *photo_id = [self photoIdFromPhoto:photo];
    NSLog(@"photo_id: %@", photo_id);
    if (photo_id) {
        BOOL hasVoted = [[NSUserDefaults standardUserDefaults] boolForKey:photo_id];
        [self ratingEnabled:!hasVoted];
    }
}


@end
