//
//  Tip.h
//  project3
//
//  Created by Edouard GODFREY on 6/8/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Tip : NSObject

@property (nonatomic,assign) NSInteger uid;
@property (nonatomic,copy) NSString *text;
@property (nonatomic,assign) NSInteger ratingPlus;
@property (nonatomic,assign) NSInteger ratingMinus;
@property (nonatomic,copy) NSString *author;
@property (nonatomic,strong) NSDate *date;

+ (id)tipWithId:(NSInteger)_id text:(NSString *)text plus:(NSInteger)plus minus:(NSInteger)minus author:(NSString *)author date:(double)secs;

@end
