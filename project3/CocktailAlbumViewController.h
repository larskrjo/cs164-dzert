//
//  ImagePickerViewController.h
//  project3
//
//  Created by Edouard GODFREY on 6/6/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import <Three20/Three20.h>

@interface CocktailAlbumViewController : TTThumbsViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic, strong) UIImagePickerController *imgPicker;

- (id)initWithChooseOption:(NSInteger)option andCocktailId:(NSInteger)cid;

@end
