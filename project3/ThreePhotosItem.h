//
//  ThreePhotosItem.h
//  project3
//
//  Created by Edouard GODFREY on 6/14/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import <Three20/Three20.h>

@interface ThreePhotosItem : TTTableLinkedItem

@property (nonatomic, copy) NSString *image1;
@property (nonatomic, copy) NSString *image2;
@property (nonatomic, copy) NSString *image3;

+ (id)itemWithImage1:(NSString *)image1
              image2:(NSString *)image2
              image3:(NSString *)image3;

+ (id)itemWithImages:(NSArray *)images;
 
@end

@interface ThreePhotosItemCell : TTTableLinkedItemCell

@property (nonatomic,strong) TTImageView *imageView1;
@property (nonatomic,strong) TTImageView *imageView2;
@property (nonatomic,strong) TTImageView *imageView3;

@end  
