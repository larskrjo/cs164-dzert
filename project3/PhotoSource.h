#import <Three20/Three20.h>

///////////////////////////////////////////////////////////////////////////////////////////////////
// (TTPhotoSource = array of photos)
@interface PhotoSource : TTURLRequestModel <TTPhotoSource> {
    BOOL _loadStarted;
}

@property (nonatomic, copy) NSString *title;
@property (nonatomic, strong) NSMutableArray *photos;
@property (nonatomic, assign) NSInteger cocktailId;

- (id)initWithTitle:(NSString *)title andCocktailId:(NSInteger)cocktailId;
- (void)addPhotoPath:(NSString *)relativePath;


@end

///////////////////////////////////////////////////////////////////////////////////////////////////

@interface Photo : NSObject <TTPhoto>

@property (nonatomic, copy) NSString *caption;
@property (nonatomic, copy) NSString *urlLarge;
@property (nonatomic, copy) NSString *urlSmall;
@property (nonatomic, copy) NSString *urlThumb;
@property (nonatomic, weak) id <TTPhotoSource> photoSource;
@property (nonatomic, assign) CGSize size;
@property (nonatomic, assign) NSInteger index;

- (id)initWithCaption:(NSString *)caption urlLarge:(NSString *)urlLarge urlSmall:(NSString *)urlSmall urlThumb:(NSString *)urlThumb size:(CGSize)size;

@end
