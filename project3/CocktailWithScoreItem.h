//
//  CocktailWithScoreItem.h
//  project3
//
//  Created by Edouard GODFREY on 6/16/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import <Three20/Three20.h>

typedef enum {
    CocktailWithScoreItemTypeRedStar,
    CocktailWithScoreItemTypeYellowStar,
    CocktailWithScoreItemTypeGreenBar,
    CocktailWithScoreItemTypeBlueStar
} CocktailWithScoreItemType;

@interface CocktailWithScoreItem : TTTableSubtitleItem

@property (nonatomic, assign) double score;
@property (nonatomic, assign) CocktailWithScoreItemType type;

+ (id)itemWithText:(NSString*)text 
          subtitle:(NSString *)subtitle
               URL:(NSString*)url
             score:(double)score
              type:(CocktailWithScoreItemType)type;

@end

////////////////////////////////////////////////////////////  
//////   CocktailWithScoreItemCell     //  
////////////////////////////////////////////////////////////  

@interface CocktailWithScoreItemCell : TTTableSubtitleItemCell

@property (nonatomic, strong) UIImageView *score;
@property (nonatomic, strong) UILabel *lblScore;

@end  
