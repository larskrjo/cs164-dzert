//
//  ThreePhotosItem.m
//  project3
//
//  Created by Edouard GODFREY on 6/14/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import <Three20/Three20.h>
#import "ThreePhotosItem.h"
#import "Three20/Three20+Additions.h"
#import "Constants.h"

@interface ThreePhotosItem ()
- (NSString*)fullPathFromRelative:(NSString *)path;
@end

@implementation ThreePhotosItem

@synthesize image1=_image1;
@synthesize image2=_image2;
@synthesize image3=_image3;

//////////////////////////////////////////////////////////////////////////////////////////
// private

- (NSString*)fullPathFromRelative:(NSString *)path {
    return [[BASE_URL stringByAppendingFormat:@"photos/uploaded_photos/%@",path] stringByAppendingString:@"_small.png"];
}

//////////////////////////////////////////////////////////////////////////////////////////
// public
+ (id)itemWithImage1:(NSString *)image1
              image2:(NSString *)image2
              image3:(NSString *)image3 {
    ThreePhotosItem *item = [[self alloc] init]; 
    if (item) {
        item.image1 = [item fullPathFromRelative:image1];
        item.image2 = [item fullPathFromRelative:image2];
        item.image3 = [item fullPathFromRelative:image3];
    }
    return item;
}

+ (id)itemWithImages:(NSArray *)images {
    ThreePhotosItem *item = [[self alloc] init]; 
    if (item) {
        NSLog(@"path: %@", [item fullPathFromRelative:[images objectAtIndex:0]]);
        if (images.count == 1)
            item.image2 = [item fullPathFromRelative:[images objectAtIndex:0]];
        if (images.count > 1) {
            item.image1 = [item fullPathFromRelative:[images objectAtIndex:0]];
            item.image2 = [item fullPathFromRelative:[images objectAtIndex:1]];
        } 
        if (images.count > 2) {
            item.image3 = [item fullPathFromRelative:[images objectAtIndex:2]];
        }
    }
    return item;
}

////////////////////////////////////////////////////////////////////////////////////////// 
// NSObject  

- (id)init {  
    if (self = [super init]) {  
        _image1 = nil;  
        _image2 = nil;  
        _image3 = nil;
    }  
    return self;  
}  

@end

///////////////////////////////////////////////////////////////////////////////////////////////////  

static CGFloat kHPadding = 10;  
static CGFloat kVPadding = 15;  
static CGFloat kImageWidth = 80;  
static CGFloat kImageHeight = 80;  

@implementation ThreePhotosItemCell

@synthesize imageView1=_imageView1, imageView2=_imageView2, imageView3=_imageView3;


+ (CGFloat)tableView:(UITableView*)tableView rowHeightForObject:(id)item {
    return kVPadding*2 + kImageHeight;  
}  

///////////////////////////////////////////////////////////////////////////////////////////////////  

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)identifier {  
    if (self = [super initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:identifier]) { 
        
        _imageView1 = [[TTImageView alloc] initWithFrame:CGRectMake(20, kVPadding, kImageWidth, kImageHeight)]; 
        _imageView1.style = TTSTYLE(rounded);
        _imageView1.backgroundColor = [UIColor clearColor];
        _imageView1.hidden = YES;
        [self.contentView addSubview:_imageView1];  
        
        _imageView2 = [[TTImageView alloc] initWithFrame:CGRectMake(_imageView1.right + kHPadding, kVPadding, kImageWidth, kImageHeight)];  
        _imageView2.style = TTSTYLE(rounded);
        _imageView2.backgroundColor = [UIColor clearColor];
        _imageView1.hidden = YES;
        [self.contentView addSubview:_imageView2];  
        
        _imageView3 = [[TTImageView alloc] initWithFrame:CGRectMake(_imageView2.right + kHPadding, kVPadding, kImageWidth, kImageHeight)];  
        _imageView3.style = TTSTYLE(rounded);
        _imageView3.backgroundColor = [UIColor clearColor];
        _imageView3.hidden = YES;
        [self.contentView addSubview:_imageView3]; 
        
    }  
    return self;  
}  

///////////////////////////////////////////////////////////////////////////////////////////////////  
// UIView  

- (void)layoutSubviews {  
    [super layoutSubviews];  
    
}  

///////////////////////////////////////////////////////////////////////////////////////////////////  
// TTTableViewCell 

- (id)object {  
    return _item;  
}

- (void)setObject:(id)object {  
    if (_item != object) {  
        [super setObject:object];  
        
        ThreePhotosItem* item = object;  
        
        _imageView1.urlPath = item.image1;  
        if (item.image1)
            _imageView1.hidden = NO;
        
        _imageView2.urlPath = item.image2;  
        if (item.image2)
            _imageView2.hidden = NO;
        
        _imageView3.urlPath = item.image3;  
        if (item.image3)
            _imageView3.hidden = NO;
    }  
}  

@end
