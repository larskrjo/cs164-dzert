//
//  Cocktail.h
//  project3
//
//  Created by Edouard GODFREY on 6/5/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Cocktail : NSObject

@property (nonatomic, assign) NSInteger uid;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *description;
@property (nonatomic, assign) double score;

+ (id)cocktailWithID:(NSInteger)uid andName:(NSString *)name andDescription:(NSString *)description;
+ (id)cocktailWithID:(NSInteger)uid andName:(NSString *)name andDescription:(NSString *)description andScore:(double)score;

@end
