//
//  TabBarControllerViewController.m
//  project3
//
//  Created by Edouard Godfrey and Lars Kristian Johansen on 4/13/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import "TabBarController.h"
#import "Three20UI/Three20UI+Additions.h" 

@interface TabBarController ()

@end

@implementation TabBarController

- (void)viewDidLoad {
    [self setTabURLs:[NSArray arrayWithObjects:
                      @"tt://cocktails",
                      @"tt://fridge",
                      @"tt://suggestions",
                      nil]];
    UITabBarItem *browse = [self.tabBar.items objectAtIndex:0];
    browse.image = [UIImage imageNamed:@"search"];
    UITabBarItem *ingredients = [self.tabBar.items objectAtIndex:1];
    ingredients.image = [UIImage imageNamed:@"ingredients"];
    UITabBarItem *suggestions = [self.tabBar.items objectAtIndex:2];
    suggestions.image = [UIImage imageNamed:@"suggestion"];
}

@end
