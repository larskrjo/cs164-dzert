//
//  AlbumPhotoViewController.h
//  project3
//
//  Created by Edouard GODFREY on 6/14/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import <Three20/Three20.h>

@interface AlbumPhotoViewController : TTPhotoViewController

@property (nonatomic,strong) UIBarButtonItem* cool;
@property (nonatomic,strong) UIBarButtonItem* notcool;

@end
