//
//  IngredientViewController.m
//  project3
//
//  Created by Edouard Godfrey and Lars Kristian Johansen on 4/10/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import "IngredientViewController.h"
#import "IngredientDataSource.h"
#import "Three20/Three20+Additions.h"
#import "Database.h"
#import "Ingredient.h"
#import "IngredientItem.h"
/*
@interface SearchTableViewController : TTTableViewController

@property (nonatomic, weak) id<IngredientViewControllerDelegate> delegate;

@end

@implementation SearchTableViewController
@synthesize delegate=_delegate;



@end*/

@implementation IngredientViewController

@synthesize delegate=_delegate;

///////////////////////////////////////////////////////////////////////////////////////////////////
// NSObject

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        self.title = @"Ingredients";
        self.dataSource = [[IngredientDataSource alloc] init];
        self.delegate = self;
    }
    return self;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// UIViewController

- (void)loadView {
    [super loadView];
    TTTableViewController* searchController = [[TTTableViewController alloc] init];
    searchController.dataSource = [[IngredientSearchDataSource alloc] init];
    self.searchViewController = searchController;
    self.tableView.tableHeaderView = _searchController.searchBar;
    _searchController.searchResultsDelegate = self;
    _searchController.searchBar.delegate = self;
}

- (void)didSelectObject:(id)object atIndexPath:(NSIndexPath*)indexPath {
    IngredientItemCell *cell = (IngredientItemCell *) [self.tableView cellForRowAtIndexPath:indexPath];
    [_delegate searchIngredientViewController:self didSelectObject:cell];
}

/// UITableViewDelegate
- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    IngredientItemCell *cell = (IngredientItemCell *) [tableView cellForRowAtIndexPath:indexPath];
    [_delegate searchIngredientViewController:self didSelectObject:cell];
    
}

// UISearchBarDelegate
- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar {
    [self.tableView reloadData];
}

// IngredientViewControllerDelegate
- (void)searchIngredientViewController:(IngredientViewController*)controller didSelectObject:(id)object {
    IngredientItemCell *cell = object;
    IngredientItem *item = [cell object];
    // switch db
    [[Database singleton] setSelected:1-item.ingredient.selected forIngredientId:item.ingredient.uid];
    // switch checkmark
    [cell switchSelection];
    // switch in datasource (avoid reloading)
    IngredientSearchDataSource *dataSource = self.searchViewController.dataSource;
    [dataSource.ingredientBook switchIngredientId:item.ingredient.uid];
    IngredientDataSource *searchDataSource = self.dataSource;
    [searchDataSource.ingredientBook switchIngredientId:item.ingredient.uid];
    
}

@end
