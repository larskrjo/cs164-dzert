//
//  DetailViewController.h
//  project3
//
//  Created by Edouard GODFREY on 6/3/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import <Three20/Three20.h>
#import "StarRatingItem.h"

typedef enum {
    DetailViewControllerTypeStandard = 1,
    DetailViewControllerTypeRedMissingIngredients = 2
} DetailViewControllerType;

@interface DetailViewController : TTTableViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate,TTPostControllerDelegate,TTAlertViewControllerDelegate,StarRatingItemDelegate> {
    
    NSInteger _cid;

}

- (id)initWithCocktailId:(NSInteger)cid;
- (id)initWithCocktailId:(NSInteger)cid type:(DetailViewControllerType)type;

@end