//
//  Constants.m
//  project3
//
//  Created by Edouard GODFREY on 6/3/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import "Constants.h"

@implementation Constants

NSString *const BASE_URL = @"http://localhost:8888/";
NSString *const GOOGLE_QUERY = @"http://www.google.com/search?tbm=isch&q=";

int PICK_CAMERA = 0;
int PICK_ALBUM = 1;
int MAX_LIST_SIZE = 30;

@end
