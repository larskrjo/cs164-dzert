//
//  IngredientDataSource.h
//  project3
//
//  Created by Edouard GODFREY on 6/15/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import <Three20/Three20.h>

/*
 * a searchable model which can be configured with a 
 * loading and/or search time
 */
@interface IngredientBook : NSObject <TTModel> {
    NSMutableArray* _delegates;
    BOOL _loadingStarted;
    BOOL _loadedDB;
    BOOL _fromFridge; // FALSE => all, TRUE => only selected
}

@property(nonatomic,strong) NSMutableArray* ingredients;
@property(nonatomic,copy) NSArray* allIngredients;

- (id)initFromFridge:(BOOL)fromFridge;
- (void)search:(NSString*)text;
- (void)switchIngredientId:(NSInteger)iid;

@end

@interface IngredientDataSource : TTSectionedDataSource

@property(nonatomic,strong) IngredientBook* ingredientBook;

- (id)init;

@end

@interface IngredientSearchDataSource : TTSectionedDataSource

@property(nonatomic,strong) IngredientBook* ingredientBook;
- (id)init;

@end

@protocol SwipeDeleteDelegate <NSObject>
-(void)         tableView:(UITableView *)tableView 
   swipeDeleteAtIndexPath:(NSIndexPath *)indexPath;
@end

@interface IngredientFridgeDataSource : IngredientSearchDataSource

@property (nonatomic,weak) id<SwipeDeleteDelegate> swipeDeleteDelegate;

@end