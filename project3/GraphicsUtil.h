//
//  GraphicsUtil.h
//  project3
//
//  Created by Edouard Godfrey and Lars Kristian Johansen on 5/2/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GraphicsUtil : NSObject

+ (UIView *)composeImageWithImage1:(UIImage *)im1 
                         andImage2:(UIImage *)im2
               withProportionOfIm1:(double)proportion
                    andTotalNumber:(NSInteger)total;

+ (UIImage *)composeBisImageWithImage1:(UIImage *)im1 
                             andImage2:(UIImage *)im2
                   withProportionOfIm1:(double)proportion
                        andTotalNumber:(NSInteger)total;

+ (UIImage*)crop:(UIImage *)imageToCrop
        withRect:(CGRect)rect;

+ (UIImage*)imageWithImage:(UIImage*)image 
              scaledToSize:(CGSize)newSize;

@end
