//
//  IngredientItemCell.m
//  project3
//
//  Created by Edouard GODFREY on 6/16/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import "IngredientItem.h"
#import "Ingredient.h"

@implementation IngredientItem
/*
@synthesize uid=_uid, score=_score, selected=_selected;

+ (id)itemWithText:(NSString*)text 
               uid:(NSInteger)uid
          selected:(NSInteger)selected {
    IngredientItem *item = [[IngredientItem alloc] init];
    if (item) {
        item.text = text;
        item.uid = uid;
        item.selected = selected;
    }
    return item;
}*/
@synthesize ingredient=_ingredient;

+ (id)itemWithIngredient:(Ingredient *)ingredient {
    IngredientItem *item = [[IngredientItem alloc] init];
    if (item) {
        item.text = ingredient.name;
        item.ingredient = ingredient;
    }
    return item;
}

@end

@implementation IngredientItemCell

- (void)setObject:(id)object {  
    if (_item != object) {
        [super setObject:object];
        IngredientItem *item = object;
        if (item.ingredient.selected == 1) {
            self.accessoryType = UITableViewCellAccessoryCheckmark;
        } else {
            self.accessoryType = UITableViewCellAccessoryNone;
        }
    }  
}

- (void)switchSelection {
    if (self.accessoryType == UITableViewCellAccessoryNone) {
        self.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        self.accessoryType = UITableViewCellAccessoryNone;
    }
}

@end
