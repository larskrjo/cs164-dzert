//
//  StarRatingItem.h
//  project3
//
//  Created by Edouard GODFREY on 6/15/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import <Three20/Three20.h>

@protocol StarRatingItemDelegate <NSObject>
- (void)starClicked:(NSInteger)newScore;
@end

@interface StarRatingItem : TTTableLinkedItem

@property (nonatomic,assign) NSInteger score;
@property (nonatomic,weak) id<StarRatingItemDelegate> starDelegate;

+ (id)itemWithScore:(NSInteger)score;

@end


@interface StarRatingItemCell : TTTableLinkedItemCell

@property (nonatomic,strong) NSMutableArray *stars;
@property (nonatomic,weak) id<StarRatingItemDelegate> starDelegate;


@end  