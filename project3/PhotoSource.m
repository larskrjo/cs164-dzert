#import "PhotoSource.h"
#import "Three20/Three20+Additions.h"
#import "Constants.h"
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"

#define GET_PHOTOS 0

@interface PhotoSource ()

@end

@implementation PhotoSource
@synthesize title = _title;
@synthesize photos = _photos;
@synthesize cocktailId=_cocktailId;

///////////////////////////////////////////////////////////////////////////////////////////////////
// public
- (void)addPhotoPath:(NSString *)relativePath {
    NSString *abs_path = [[BASE_URL stringByAppendingString:@"photos/uploaded_photos/"] stringByAppendingString:relativePath];
    Photo *new = [[Photo alloc] 
                  initWithCaption:nil
                  urlLarge:[abs_path stringByAppendingString:@"_big.png"]
                  urlSmall:[abs_path stringByAppendingString:@"_small.png"]
                  urlThumb:[abs_path stringByAppendingString:@"_thumb.png"]
                  size:CGSizeMake(320, 320)];
    new.photoSource = self;
    new.index = [self numberOfPhotos];
    [self.photos addObject:new];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// ASI

//static TTURLRequestCachePolicy _cachePolicy;

- (void)requestFinished:(ASIHTTPRequest *)request
{
    NSString *responseString = [request responseString];
    if (request.tag == GET_PHOTOS) {
        NSError *e = nil;
        NSData *data = [responseString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data 
                                                                     options: NSJSONReadingMutableContainers error: &e];
        NSString *status = [responseDict objectForKey:@"status"];
        NSMutableArray *photos = [NSMutableArray array];
        NSInteger i = 0;
        if ([status isEqualToString:@"ok"]) {
            for (NSString *relativePath in [responseDict objectForKey:@"paths"]) {
                NSString *abs_path = [[BASE_URL stringByAppendingString:@"photos/uploaded_photos/"] stringByAppendingString:relativePath];
                Photo *new = [[Photo alloc] 
                              initWithCaption:nil
                              urlLarge:[abs_path stringByAppendingString:@"_big.png"]
                              urlSmall:[abs_path stringByAppendingString:@"_small.png"]
                              urlThumb:[abs_path stringByAppendingString:@"_thumb.png"]
                              size:CGSizeMake(320, 320)];
                new.photoSource = self;
                new.index = i++;
                [photos addObject:new];
            }
        }
        self.photos = photos;
        [_delegates perform:@selector(modelDidFinishLoad:) withObject:self];
    }
}

- (void)requestFailed:(ASIHTTPRequest *)request
{    
    NSError *error = [request error];
    NSLog(@"Error: %@", error);
    if (request.tag == GET_PHOTOS) {
        NSLog(@"Couldn't get photos paths from cloud");
    }
    _photos = [NSMutableArray array];
    for (id delegate in _delegates) {
        [delegate performSelectorOnMainThread:@selector(modelDidFinishLoad:) withObject:self waitUntilDone:NO];
    }
}

- (id)initWithTitle:(NSString *)title andCocktailId:(NSInteger)cocktailId {
    if (self = [super init]) {
        self.title = title;
        _cocktailId = cocktailId;
        _photos = nil;
    }
    return self;
}

#pragma mark TTModel

- (BOOL)isLoading { 
    return _loadStarted && ![self isLoaded];
}

- (BOOL)isLoaded {
    return !!_photos;
}

- (void)load:(TTURLRequestCachePolicy)cachePolicy more:(BOOL)more {
    NSLog(@"load:cachePolicy:more");
    //_cachePolicy = cachePolicy;
    _loadStarted = YES;
    [_delegates perform:@selector(modelDidStartLoad:) withObject:self];
    // query the internet for photos
    NSLog(@"Getting photo paths from cloud for cocktail_id: %d", _cocktailId);
    NSURL *url = [NSURL URLWithString:[BASE_URL stringByAppendingString:@"photos/get.php"] ];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    request.tag = GET_PHOTOS;
    [request setPostValue:[NSNumber numberWithInt:_cocktailId] 
                   forKey:@"cocktail_id" ];
    [request setDelegate:self];
    [request startAsynchronous];
}

#pragma mark TTPhotoSource

- (NSInteger)numberOfPhotos {
    return _photos.count;
}

- (NSInteger)maxPhotoIndex {
    return _photos.count-1;
}

- (id<TTPhoto>)photoAtIndex:(NSInteger)photoIndex {
    if (photoIndex < _photos.count) {
        return [_photos objectAtIndex:photoIndex];
    } else {
        return nil;
    }
}

@end

///////////////////////////////////////////////////////////////////////////////////////////////////

@implementation Photo

@synthesize caption = _caption;
@synthesize urlLarge = _urlLarge;
@synthesize urlSmall = _urlSmall;
@synthesize urlThumb = _urlThumb;
@synthesize photoSource = _photoSource;
@synthesize size = _size;
@synthesize index = _index;

- (id)initWithCaption:(NSString *)caption urlLarge:(NSString *)urlLarge urlSmall:(NSString *)urlSmall urlThumb:(NSString *)urlThumb size:(CGSize)size {
    if ((self = [super init])) {
        self.caption = caption;
        self.urlLarge = urlLarge;
        self.urlSmall = urlSmall;
        self.urlThumb = urlThumb;
        self.size = size;
        self.index = NSIntegerMax;
        self.photoSource = nil;
    }
    return self;
}

#pragma mark TTPhoto

- (NSString*)URLForVersion:(TTPhotoVersion)version {
    switch (version) {
        case TTPhotoVersionLarge:
            return _urlLarge;
        case TTPhotoVersionMedium:
            return _urlLarge;
        case TTPhotoVersionSmall:
            return _urlSmall;
        case TTPhotoVersionThumbnail:
            return _urlThumb;
        default:
            return nil;
    }
}

@end