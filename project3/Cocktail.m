//
//  Cocktail.m
//  project3
//
//  Created by Edouard GODFREY on 6/5/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import "Cocktail.h"

@implementation Cocktail

@synthesize uid=_uid, name=_name, description=_description, score=_score;

+ (id)cocktailWithID:(NSInteger)uid andName:(NSString *)name andDescription:(NSString *)description {
    Cocktail *newcocktail = [[self alloc] init];
    if (newcocktail) {
	newcocktail.description = description;
	newcocktail.name = name;
    newcocktail.uid = uid;
    }
	return newcocktail;
}

+ (id)cocktailWithID:(NSInteger)uid andName:(NSString *)name andDescription:(NSString *)description andScore:(double)score {
    Cocktail *newcocktail = [[self alloc] init];
    if (newcocktail) {
        newcocktail.description = description;
        newcocktail.name = name;
        newcocktail.uid = uid;
        newcocktail.score = score;
    }
	return newcocktail;
}

@end
