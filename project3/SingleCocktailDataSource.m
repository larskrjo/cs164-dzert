//
//  SingleCocktailDataSource.m
//  project3
//
//  Created by Edouard GODFREY on 6/6/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import "SingleCocktailDataSource.h"
#import "Three20/Three20+Additions.h"
#import "Database.h"
#import "Constants.h"
#import "ASIFormDataRequest.h"
#import "ASIHTTPRequest.h"
#import "Tip.h"
#import "TipItem.h"
#import "ThreePhotosItem.h"
#import "StarRatingItem.h"
#import "IngredientQuantityItem.h"
#import "Ingredient.h"

#define GET_PHOTOS 0
#define GET_TIPS 1
#define COMMENTS_INDEX 3

@implementation SingleCocktail

@synthesize cocktail=_cocktail, 
ingredients=_ingredients, 
tips=_tips, 
photoPaths=_photoPaths,
ingredientsQuantities=_ingredientsQuantities,
recipe=_recipe,
score=_score;

///////////////////////////////////////////////////////////////////////////////////////////////////
// private

- (void)loadFromDBAndInternet {
    self.ingredients = [[Database singleton] ingredientByCocktailId:_cocktail.uid];
    self.ingredientsQuantities = [[Database singleton] ingredientQuantitiesByCocktailId:_cocktail.uid];
    self.recipe = [[Database singleton] recipeByCocktailId:_cocktail.uid];
    _cocktail.name = [[Database singleton] cocktailNameFromID:_cocktail.uid];
    self.score = [[Database singleton] getCocktailScore:_cocktail.uid];
    //NSInteger score = [[Database singleton] getCocktailScore:_cid];
    NSLog(@"score read for %d: %d", _cocktail.uid, self.score);

    _loadedDB = YES;
    
    // query the internet for tips
    NSLog(@"Getting photo paths from cloud for cocktai_id: %d", _cocktail.uid);
    NSURL *url = [NSURL URLWithString:[BASE_URL stringByAppendingString:@"tips/get.php"] ];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    request.tag = GET_TIPS;
    [request setPostValue:[NSNumber numberWithInt:_cocktail.uid] 
                   forKey:@"cocktail_id" ];
    [request setDelegate:self];
    [request startAsynchronous];
    
    // query the internet for photos
    NSLog(@"Getting photo paths from cloud for cocktai_id: %d", _cocktail.uid);
    NSURL *url2 = [NSURL URLWithString:[BASE_URL stringByAppendingString:@"photos/get.php"] ];
    ASIFormDataRequest *request2 = [ASIFormDataRequest requestWithURL:url2];
    request2.tag = GET_PHOTOS;
    [request2 setPostValue:[NSNumber numberWithInt:_cocktail.uid] 
                   forKey:@"cocktail_id" ];
    [request2 setDelegate:self];
    [request2 startAsynchronous];
    
    NSLog(@"modelDidFinishLoad");
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// ASI

- (void)requestFinished:(ASIHTTPRequest *)request
{
    NSString *responseString = [request responseString];
    NSLog(@"Response: %@", responseString);
    NSError *e = nil;
    NSString *status;
    NSData *data = [responseString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data 
                                                                 options: NSJSONReadingMutableContainers error: &e];
    switch (request.tag) {
        case GET_PHOTOS:
            status = [responseDict objectForKey:@"status"];
            if ([status isEqualToString:@"ok"]) {
                self.photoPaths = [responseDict objectForKey:@"paths"];
            }
            _loadedPhotoPaths = YES;
            break;
            
        case GET_TIPS:
            status = [responseDict objectForKey:@"status"];
            if ([status isEqualToString:@"ok"]) {
                NSArray *tipText = [responseDict objectForKey:@"text"];
                NSArray *tipId = [responseDict objectForKey:@"id"];
                NSArray *plus = [responseDict objectForKey:@"plus"];
                NSArray *minus = [responseDict objectForKey:@"minus"];
                NSArray *authors = [responseDict objectForKey:@"login"];
                NSArray *dates = [responseDict objectForKey:@"date"];
                self.tips = [NSMutableArray array];
                for (int i = 0; i < tipText.count; ++i) {
                    [self.tips addObject:[Tip tipWithId:[[tipId objectAtIndex:i] intValue] 
                                                   text:[tipText objectAtIndex:i] 
                                                   plus:[[plus objectAtIndex:i] intValue] 
                                                  minus:[[minus objectAtIndex:i] intValue]
                                                 author:[authors objectAtIndex:i]
                                                   date:[[dates objectAtIndex:i] doubleValue]]];
                }
            }
            _loadedTips = YES;
            break;
    }
    if ([self isLoaded]) {
        for (id delegate in _delegates) {
            [delegate performSelectorOnMainThread:@selector(modelDidFinishLoad:) withObject:self waitUntilDone:NO];
        }
    }
}

- (void)requestFailed:(ASIHTTPRequest *)request
{    
    NSError *error = [request error];
    NSLog(@"Error: %@", error);
    switch (request.tag) {
        case GET_PHOTOS:
            NSLog(@"Couldn't get photos paths from cloud");
            _loadedPhotoPaths = YES;
            break;
        case GET_TIPS:
            NSLog(@"Couldn't get tips from cloud");
            _loadedTips = YES;
            break;
    }
    if ([self isLoaded]) {
        for (id delegate in _delegates) {
            [delegate performSelectorOnMainThread:@selector(modelDidFinishLoad:) withObject:self waitUntilDone:NO];
        }
    }
}




///////////////////////////////////////////////////////////////////////////////////////////////////
// NSObject

- (id)initWithCocktailId:(NSInteger)cid {
    if (self = [super init]) {
        _delegates = nil;
        _cocktail = [[Cocktail alloc] init];
        _cocktail.uid = cid;
        _ingredients = nil;
        _ingredientsQuantities = nil;
        _tips = nil;
        _photoPaths = nil;
        _loadedDB = NO;
        _loadedTips = NO;
        _loadedPhotoPaths = NO;
        _loadingStarted = NO;
    }
    return self;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// TTModel

- (NSMutableArray*)delegates {
    if (!_delegates) {
        _delegates = TTCreateNonRetainingArray();
    }
    return _delegates;
}

- (BOOL)isLoadingMore {
    return NO;
}

- (BOOL)isOutdated {
    return NO;
}

- (BOOL)isLoaded {
    return _loadedDB && _loadedPhotoPaths && _loadedTips;
}

- (BOOL)isLoading {
    return _loadingStarted && ![self isLoaded];
}

- (BOOL)isEmpty {
    return !!_cocktail.name;
}

- (void)load:(TTURLRequestCachePolicy)cachePolicy more:(BOOL)more {
    NSLog(@"load:cachePolicy:more");
    [_delegates perform:@selector(modelDidStartLoad:) withObject:self];
    _loadingStarted = YES;
    NSOperationQueue *q = [[NSOperationQueue alloc] init];
    [q addOperationWithBlock:^{
        [self loadFromDBAndInternet];
    }];
}

- (void)invalidate:(BOOL)erase {
}

- (void)cancel {
    [_delegates perform:@selector(modelDidCancelLoad:) withObject:self];
}

@end

///////////////////////////////////////////////////////////////////////////////////////////////////

@implementation SingleCocktailDataSource

@synthesize cocktail=_cocktail,
    starDelegate=_starDelegate;

///////////////////////////////////////////////////////////////////////////////////////////////////
// NSObject

- (id)initWithCocktailId:(NSInteger)cid {
    if (self = [super init]) {
        _cocktail = [[SingleCocktail alloc] initWithCocktailId:cid];
        self.model = _cocktail;
    }
    return self;
}

///////////////////////////////////////////////////////////////////////////////////////////////////  
// TTTableViewDataSource  

- (Class)tableView:(UITableView*)tableView cellClassForObject:(id) object {  
    
    if ([object isKindOfClass:[TipItem class]]) {  
        return [TipItemCell class];  
    } else if ([object isKindOfClass:[ThreePhotosItem class]]) {
        return [ThreePhotosItemCell class];
    } else if ([object isKindOfClass:[StarRatingItem class]]) {
        return [StarRatingItemCell class];
    } else {  
        return [super tableView:tableView cellClassForObject:object];  
    }  
}  

- (void)        tableView:(UITableView*)tableView 
              prepareCell:(UITableViewCell*)cell  
        forRowAtIndexPath:(NSIndexPath*)indexPath {  
    cell.accessoryType = UITableViewCellAccessoryNone;  
}

////////////////////////////////

- (void)tableViewDidLoadModel:(UITableView*)tableView {
    self.items = [NSMutableArray array];
    self.sections = [NSMutableArray array];
    StarRatingItem *item = [StarRatingItem itemWithScore:_cocktail.score];
    item.starDelegate = _starDelegate;
    NSArray *sectionRatings = [NSArray arrayWithObject:item];
    
    // adding photos
    NSMutableArray *sectionPhotos = [NSMutableArray array];
    if (_cocktail.photoPaths.count > 0) {
        [sectionPhotos addObject:[ThreePhotosItem itemWithImages:_cocktail.photoPaths]];
        NSString *plural = (_cocktail.photoPaths.count > 1) ? @"s" : @"";
        [sectionPhotos addObject:[TTTableButton 
                              itemWithText:[NSString stringWithFormat:@"%d picture%@",_cocktail.photoPaths.count,plural] 
                              URL:@"tt://albumViewer"]];
    }
    [sectionPhotos addObject:[TTTableButton itemWithText:@"Add a new picture" 
                                                     URL:[NSString stringWithFormat:@"tt://photos/alertPicker/%d",_cocktail.cocktail.uid]]];
    // adding ingredients
    NSMutableArray *sectionIngredients = [NSMutableArray array];
    for (NSInteger i = 0; i < _cocktail.ingredients.count; ++i) {
        NSString *name = [[_cocktail.ingredients objectAtIndex:i] name];
        TTTableItem *item = [TTTableCaptionItem 
                             itemWithText:name 
                             caption:[_cocktail.ingredientsQuantities objectAtIndex:i]
                             URL:[GOOGLE_QUERY stringByAppendingString:[name stringByReplacingOccurrencesOfString:@" " withString:@"+"]]];
        [sectionIngredients addObject:item];
    }
    
    // adding recipe
    NSArray *sectionRecipe = [NSArray arrayWithObject:[TTTableLongTextItem 
                                                       itemWithText:_cocktail.recipe]];
    
    // adding tips
    NSMutableArray *sectionTips = [NSMutableArray array];
    for (Tip *tip in _cocktail.tips) {
        BOOL hasVoted = [[NSUserDefaults standardUserDefaults] 
                         boolForKey:[NSString stringWithFormat:@"tip_%d",tip.uid]];
        NSString *url = hasVoted ? nil : [NSString stringWithFormat:@"tt://tips/alert/%d",tip.uid];
        TTTableItem *item =     
        [TipItem itemWithTitle:tip.author
                          text:tip.text 
                     timestamp:tip.date
                           URL:url
                    ratingPlus:tip.ratingPlus 
                   ratingMinus:tip.ratingMinus
                   voteEnabled:!hasVoted];
        NSLog(@"url: %@", [NSString stringWithFormat:@"tt://tips/alert/%d",tip.uid]);
        [sectionTips addObject:item];
    }
    [sectionTips addObject:[TTTableButton itemWithText:@"Add a comment" 
                                              delegate:self 
                                              selector:@selector(addComment)]];
    
    [_sections addObject:@""];
    [_items addObject:sectionRatings];
    [_sections addObject:@""];
    [_items addObject:sectionPhotos];
    [_sections addObject:@"Ingredients"];
    [_items addObject:sectionIngredients];
    [_sections addObject:@"Recipe"];
    [_items addObject:sectionRecipe];
    [_sections addObject:@"Comments"];
    [_items addObject:sectionTips];
    
    
}

- (void)addComment {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (! [defaults stringForKey:@"login"]) {
        // login not set
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:nil message:@"Choose a log-in for Dzert platform" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Done", nil];
        alert.alertViewStyle = UIAlertViewStylePlainTextInput;
        alert.delegate = self;
        [alert show];
    } else {
        [[TTNavigator navigator] openURLAction:[TTURLAction actionWithURLPath:@"tt://tips/post"]];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex==1) { // Done
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:[alertView textFieldAtIndex:0].text forKey:@"login"];
        [[TTNavigator navigator] openURLAction:[TTURLAction actionWithURLPath:@"tt://tips/post"]];
    }
}

/*- (void)addTip:(NSInteger)tid text:(NSString *)text {
    NSMutableArray *sectionTips = [_items objectAtIndex:COMMENTS_INDEX];
    TTTableItem *item =     
    [TTTableMessageItem itemWithTitle:@"Author" 
                              caption:@"(0/0 like it)"
                                 text:text 
                            timestamp:[NSDate date]
                             imageURL:nil 
                                  URL:[NSString stringWithFormat:@"tt://tips/alert/%d",tid]];
    [sectionTips insertObject:item atIndex:0];
}

- (void)modifyTip:(NSInteger)tipId ratingPlus:(NSInteger)ratingPlus
      ratingMinus:(NSInteger)ratingMinus {
    NSMutableArray *sectionTips = [_items objectAtIndex:COMMENTS_INDEX];
    for (TTTableMessageItem *item in sectionTips) {
        if ([item.URLValue isEqualToString:[NSString stringWithFormat:@"tt://tips/alert/%d",tipId]]) {
            item.caption = [NSString stringWithFormat:@"(%d/%d like it)",ratingPlus,ratingPlus+ratingMinus];
            return;
        }
    }
    
}

- (void)addTip:(NSString *)tip {
    NSMutableArray *sectionTips = [_items objectAtIndex:COMMENTS_INDEX];
    [sectionTips insertObject:tip atIndex:0];
}*/

- (id<TTModel>)model {
    return _cocktail;
} 

@end

@implementation SingleCocktailFromFridgeDataSource

///////////////////////////////////////////////////////////////////////////////////////////////////  
// TTTableViewDataSource  

- (Class)tableView:(UITableView*)tableView cellClassForObject:(id) object {  
    
    if ([object isKindOfClass:[IngredientQuantityItem class]]) {  
        return [IngredientQuantityItemCell class];
    } else {  
        return [super tableView:tableView cellClassForObject:object];  
    }  
}  

- (void)        tableView:(UITableView*)tableView 
              prepareCell:(UITableViewCell*)cell  
        forRowAtIndexPath:(NSIndexPath*)indexPath {  
    cell.accessoryType = UITableViewCellAccessoryNone;  
}

- (void)tableViewDidLoadModel:(UITableView*)tableView {
    self.items = [NSMutableArray array];
    self.sections = [NSMutableArray array];
    StarRatingItem *item = [StarRatingItem itemWithScore:_cocktail.score];
    item.starDelegate = self.starDelegate;
    NSArray *sectionRatings = [NSArray arrayWithObject:item];
    
    // adding photos
    NSMutableArray *sectionPhotos = [NSMutableArray array];
    if (_cocktail.photoPaths.count > 0) {
        [sectionPhotos addObject:[ThreePhotosItem itemWithImages:_cocktail.photoPaths]];
        NSString *plural = (_cocktail.photoPaths.count > 1) ? @"s" : @"";
        [sectionPhotos addObject:[TTTableButton 
                                  itemWithText:[NSString stringWithFormat:@"%d picture%@",_cocktail.photoPaths.count,plural] 
                                  URL:@"tt://albumViewer"]];
    }
    [sectionPhotos addObject:[TTTableButton itemWithText:@"Add a new picture" 
                                                     URL:[NSString stringWithFormat:@"tt://photos/alertPicker/%d",_cocktail.cocktail.uid]]];
    // adding ingredients
    NSMutableArray *sectionIngredients = [NSMutableArray array];
    for (NSInteger i = 0; i < _cocktail.ingredients.count; ++i) {
        Ingredient *ing = [_cocktail.ingredients objectAtIndex:i];
        TTTableItem *item = [IngredientQuantityItem 
                             itemWithText:ing.name 
                             caption:[_cocktail.ingredientsQuantities objectAtIndex:i]
                             selected:ing.selected
                             URL:[GOOGLE_QUERY stringByAppendingString:[ing.name stringByReplacingOccurrencesOfString:@" " withString:@"+"]]];
        [sectionIngredients addObject:item];
    }
    
    // adding recipe
    NSArray *sectionRecipe = [NSArray arrayWithObject:[TTTableLongTextItem 
                                                       itemWithText:_cocktail.recipe]];
    
    // adding tips
    NSMutableArray *sectionTips = [NSMutableArray array];
    for (Tip *tip in _cocktail.tips) {
        BOOL hasVoted = [[NSUserDefaults standardUserDefaults] 
                         boolForKey:[NSString stringWithFormat:@"tip_%d",tip.uid]];
        NSString *url = hasVoted ? nil : [NSString stringWithFormat:@"tt://tips/alert/%d",tip.uid];
        TTTableItem *item =     
        [TipItem itemWithTitle:tip.author
                          text:tip.text 
                     timestamp:tip.date
                           URL:url
                    ratingPlus:tip.ratingPlus 
                   ratingMinus:tip.ratingMinus
                   voteEnabled:!hasVoted];
        NSLog(@"url: %@", [NSString stringWithFormat:@"tt://tips/alert/%d",tip.uid]);
        [sectionTips addObject:item];
    }
    [sectionTips addObject:[TTTableButton itemWithText:@"Add a comment" 
                                              delegate:self 
                                              selector:@selector(addComment)]];
    
    [_sections addObject:@""];
    [_items addObject:sectionRatings];
    [_sections addObject:@""];
    [_items addObject:sectionPhotos];
    [_sections addObject:@"Ingredients"];
    [_items addObject:sectionIngredients];
    [_sections addObject:@"Recipe"];
    [_items addObject:sectionRecipe];
    [_sections addObject:@"Comments"];
    [_items addObject:sectionTips];
    
    
}

@end
