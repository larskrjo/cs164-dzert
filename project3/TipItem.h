//
//  TableItemTextWithRating.h
//  project3
//
//  Created by Edouard GODFREY on 6/7/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//
#import <Three20/Three20.h>

@interface TipItem : TTTableMessageItem

@property (nonatomic, assign) NSInteger ratingPlus;
@property (nonatomic, assign) NSInteger ratingMinus;
@property (nonatomic, assign) BOOL voteEnabled;

+ (id)itemWithTitle:(NSString*)title
               text:(NSString*)text 
          timestamp:(NSDate*)date
                URL:(NSString*)url
         ratingPlus:(NSInteger)plus
        ratingMinus:(NSInteger) minus 
        voteEnabled:(BOOL)enabled;

@end

////////////////////////////////////////////////////////////  
//////   TTTableMessageItemCell     //  
////////////////////////////////////////////////////////////  

@interface TipItemCell : TTTableMessageItemCell {
    BOOL _selectable;
}

@property (nonatomic,strong) UIView *green;
@property (nonatomic,strong) UIView *red;
@property (nonatomic,strong) UILabel *lblRating;

@end  
