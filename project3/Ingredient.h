//
//  Ingredient.h
//  project3
//
//  Created by Edouard GODFREY on 6/15/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Ingredient : NSObject

@property (nonatomic, assign) NSInteger uid;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, assign) NSInteger selected;

+ (id)ingredientWithID:(NSInteger)uid andName:(NSString *)name selected:(NSInteger)selected;

@end
