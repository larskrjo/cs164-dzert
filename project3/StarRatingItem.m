//
//  StarRatingItem.m
//  project3
//
//  Created by Edouard GODFREY on 6/15/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import <Three20/Three20.h>
#import "StarRatingItem.h"
#import "Three20/Three20+Additions.h"

@implementation StarRatingItem

@synthesize score=_score;
@synthesize starDelegate=_starDelegate;

//////////////////////////////////////////////////////////////////////////////////////////
// public

+ (id)itemWithScore:(NSInteger)score {
    StarRatingItem *item = [[self alloc] init];
    if (item) {
        item.score = score;
    }
    return item;
}

////////////////////////////////////////////////////////////////////////////////////////// 
// NSObject  

- (id)init {  
    if (self = [super init]) {  
        _score = 0;
    }  
    return self;  
}  

@end


///////////////////////////////////////////////////////////////////////////////////////////////////  

static CGFloat kHPadding = 10;  
static CGFloat kVPadding = 0;  
static CGFloat kImageWidth = 30;  
static CGFloat kImageHeight = 30; 

#define NUMBER_STARS 5

@implementation StarRatingItemCell

@synthesize stars=_stars;
@synthesize starDelegate=_starDelegate;


+ (CGFloat)tableView:(UITableView*)tableView rowHeightForObject:(id)item {
    return kVPadding*2 + kImageHeight;  
}  

//// private
- (void)starClicked:(id)sender {
    UIButton *selected = (UIButton *)sender;
    /*UIImage *filledStar = [UIImage imageNamed:@"star_yellow_big"];
    UIImage *voidStar = [UIImage imageNamed:@"star_grey_big"];
    for (int i = 0; i < NUMBER_STARS; ++i) {
        UIButton *star = [_stars objectAtIndex:i];
        if (i < selected.tag) {
            [star setImage:filledStar forState:UIControlStateNormal];
        } else {
            [star setImage:voidStar forState:UIControlStateNormal];
        }
    }*/
    [_starDelegate starClicked:selected.tag];
}

///////////////////////////////////////////////////////////////////////////////////////////////////  

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)identifier {  
    if (self = [super initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:identifier]) { 
        self.backgroundView = [[UIView alloc] initWithFrame:CGRectZero];
        self.stars = [NSMutableArray array];
        UIImage *voidStar = [UIImage imageNamed:@"star_grey_big"];
        for (int i = 0; i < NUMBER_STARS; ++i) {
            UIButton *star = [[UIButton alloc] init];
            star.tag = i+1;
            [star addTarget:self action:@selector(starClicked:) forControlEvents:UIControlEventTouchUpInside];
            [star setImage:voidStar forState:UIControlStateNormal];
            star.top = kVPadding;
            star.size = CGSizeMake(kImageWidth,kImageHeight);
            [_stars addObject:star];
            [self.contentView addSubview:star];
        }
        UIButton *star1 = [_stars objectAtIndex:0];
        UIButton *star2 = [_stars objectAtIndex:1];
        UIButton *star3 = [_stars objectAtIndex:2];
        UIButton *star4 = [_stars objectAtIndex:3];
        UIButton *star5 = [_stars objectAtIndex:4];
        star3.left = self.contentView.center.x - kImageWidth/2.0 - 5.0;
        star2.right = star3.left-kHPadding;
        star1.right = star2.left-kHPadding;
        star4.left = star3.right+kHPadding;
        star5.left = star4.right+kHPadding;
    }  
    return self;  
}  

///////////////////////////////////////////////////////////////////////////////////////////////////  
// UIView  

- (void)layoutSubviews {  
    [super layoutSubviews];  
    
}  

///////////////////////////////////////////////////////////////////////////////////////////////////  
// TTTableViewCell 

- (id)object {  
    return _item;  
}

- (void)setObject:(id)object {  
    if (_item != object) {  
        [super setObject:object];  
        
        StarRatingItem* item = object; 
        NSLog(@"item with score: %d", item.score);
        UIImage *filledStar = [UIImage imageNamed:@"star_yellow_big"];
        
        for (int i = 0; i < item.score; ++i) {
            UIButton *star = [_stars objectAtIndex:i];
            [star setImage:filledStar forState:UIControlStateNormal];
        }
        _starDelegate = item.starDelegate;
    }  
}  

@end
