//
//  TabBarControllerViewController.h
//  project3
//
//  Created by Edouard Godfrey and Lars Kristian Johansen on 4/13/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import <Three20/Three20.h>

@interface TabBarController : UITabBarController {
}

@end
