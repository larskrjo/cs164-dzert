//
//  SuggestionViewController.m
//  project3
//
//  Created by Edouard Godfrey and Lars Kristian Johansen on 4/10/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import "Three20/Three20+Additions.h"
#import "SuggestionViewController.h"

@implementation SuggestionViewController

@synthesize tab=_tab;
@synthesize justForYou=_justForYou,alreadyRated=_alreadyRated,byPopularity=_byPopularity;

///////////////////////////////////////////////////////////////////////////////////////////////////
// NSObject

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        self.title = @"Suggestions";
        self.variableHeightRows = YES;
    }
    return self;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// UIViewController

- (void)loadView {
    [super loadView];
    CGRect applicationFrame = [UIScreen mainScreen].applicationFrame;
    _tab = [[TTTabBar alloc] initWithFrame:CGRectMake(0, 0, applicationFrame.size.width, 40)];
    _tab.tabItems = [NSArray arrayWithObjects:
                     [[TTTabItem alloc] initWithTitle:@"Your best"],
                     [[TTTabItem alloc] initWithTitle:@"Just for you"],
                     [[TTTabItem alloc] initWithTitle:@"Popularity"],
                     nil];
    _tab.selectedTabIndex = 0;
    _tab.delegate = self;
    [self.view addSubview:_tab];
    self.tableView.height -= 40;
    self.tableView.top += 40;
    
    _alreadyRated = [[CocktailListDataSource alloc] initWithCocktailBookType:CocktailBookTypeAlreadyRated type:CocktailWithScoreItemTypeYellowStar detailViewControllerType:DetailViewControllerTypeStandard];
    self.dataSource = _alreadyRated;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self reload];
}

//// TTTabBarDelegate
- (void)tabBar:(TTTabBar*)tabBar tabSelected:(NSInteger)selectedIndex {
    switch (selectedIndex) {
        case 0: // Best rated
            if (! _alreadyRated) {
                _alreadyRated = [[CocktailListDataSource alloc] initWithCocktailBookType:CocktailBookTypeAlreadyRated type:CocktailWithScoreItemTypeYellowStar detailViewControllerType:DetailViewControllerTypeStandard];
            }
            self.dataSource = _alreadyRated;
            break;
        case 1: // Suggestions
            if (! _justForYou) {
                _justForYou = [[CocktailListDataSource alloc] initWithCocktailBookType:CocktailBookTypeJustForYou type:CocktailWithScoreItemTypeRedStar detailViewControllerType:DetailViewControllerTypeStandard];
            }
            self.dataSource = _justForYou;
            break;
        case 2: // By popularity
            if (! _byPopularity) {
                _byPopularity = [[CocktailListDataSource alloc] initWithCocktailBookType:CocktailBookTypeByPopularity type:CocktailWithScoreItemTypeBlueStar detailViewControllerType:DetailViewControllerTypeStandard];
            }
            self.dataSource = _byPopularity;
            break;
    }
}

@end
