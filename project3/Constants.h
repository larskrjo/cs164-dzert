//
//  Constants.h
//  project3
//
//  Created by Edouard GODFREY on 6/3/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Constants : NSObject

FOUNDATION_EXPORT NSString *const BASE_URL;
FOUNDATION_EXPORT NSString *const GOOGLE_QUERY;

FOUNDATION_EXPORT int PICK_CAMERA;
FOUNDATION_EXPORT int PICK_ALBUM;
FOUNDATION_EXPORT int MAX_LIST_SIZE;

@end
