//
//  BrowseViewController.h
//  project3
//
//  Created by Edouard Godfrey and Lars Kristian Johansen on 4/10/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import <Three20/Three20.h>
/**
 * @interface BrowseViewController The view where you can browse the cocktails.
 */

@protocol BrowseViewControllerDelegate;
@class CocktailDataSource;

@interface BrowseViewController : TTTableViewController <TTSearchTextFieldDelegate> {
    id<BrowseViewControllerDelegate> _delegate;
}

@property(nonatomic,strong) id<BrowseViewControllerDelegate> delegate;

@end

@protocol BrowseViewControllerDelegate <NSObject>

- (void)searchBrowseViewController:(BrowseViewController*)controller didSelectObject:(id)object;

@end
