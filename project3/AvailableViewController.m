//
//  AvailableViewController.m
//  project3
//
//  Created by Edouard GODFREY on 6/16/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import "AvailableViewController.h"
#import "CocktailDataSource.h"
#import "Three20/Three20+Additions.h"
#import "CocktailWithScoreItem.h"
#import "DetailViewController.h"

@implementation AvailableViewController

///////////////////////////////////////////////////////////////////////////////////////////////////
// NSObject

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        self.title = @"Available cocktails";
        self.variableHeightRows = YES;
        self.hidesBottomBarWhenPushed = YES;
    }
    return self;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// UIViewController

- (void)loadView {
    [super loadView];
    self.dataSource = [[CocktailListDataSource alloc] initWithCocktailBookType:CocktailBookTypeFromFridge type:CocktailWithScoreItemTypeGreenBar detailViewControllerType:DetailViewControllerTypeRedMissingIngredients];
}

@end
