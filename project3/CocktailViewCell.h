//
//  CocktailViewCell.h
//  project3
//
//  Created by Edouard Godfrey and Lars Kristian Johansen on 5/2/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 * @interface CocktailViewCell The view cell for a cocktail in the table.
 */
@interface CocktailViewCell : UITableViewCell

/**
 * @brief The name of the cocktail
 */
@property (nonatomic, strong) IBOutlet UILabel *name;
/**
 * @brief The ingredients.
 */
@property (nonatomic, strong) IBOutlet UILabel *ingredients;
/**
 * @brief The star image.
 */
@property (nonatomic, strong) IBOutlet UIImageView *starImage;

+ (CGFloat)height;


@end
