//
//  CocktailViewCell.m
//  project3
//
//  Created by Edouard Godfrey and Lars Kristian Johansen on 5/2/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import "CocktailViewCell.h"

@implementation CocktailViewCell

@synthesize name=_name;
@synthesize ingredients=_ingredients;
@synthesize starImage=_starImage;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (NSString *) reuseIdentifier {
    return @"CocktailViewCell";
}

+ (CGFloat)height {
    return 120.0f;
}

@end
