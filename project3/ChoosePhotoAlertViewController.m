//
//  ChoosePhotoAlertViewController.m
//  project3
//
//  Created by Edouard GODFREY on 6/6/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import "ChoosePhotoAlertViewController.h"
#import "Constants.h"

@interface ChoosePhotoAlertViewController ()

@end

@implementation ChoosePhotoAlertViewController

- (id)initWithCocktailId:(NSInteger)cid {
    self = [super init];
    if (self) {
        [self 
         addButtonWithTitle:@"Pick an existing one" 
         URL:[NSString stringWithFormat:@"tt://photos/picker/%d/%d",PICK_ALBUM,cid]];
        [self 
         addButtonWithTitle:@"Take a new one" 
         URL:[NSString stringWithFormat:@"tt://photos/picker/%d/%d",PICK_CAMERA,cid]];
        [self addCancelButtonWithTitle:@"Cancel" URL:nil];
    }
    return self;
}

@end
