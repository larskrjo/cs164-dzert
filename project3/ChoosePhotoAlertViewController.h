//
//  ChoosePhotoAlertViewController.h
//  project3
//
//  Created by Edouard GODFREY on 6/6/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import <Three20/Three20.h>

@interface ChoosePhotoAlertViewController : TTAlertViewController

- (id)initWithCocktailId:(NSInteger)cid;

@end
