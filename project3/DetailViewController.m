//
//  DetailViewController.m
//  project3
//
//  Created by Edouard GODFREY on 6/3/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import "DetailViewController.h"
#import "SingleCocktailDataSource.h"
#import "Three20/Three20+Additions.h"
#import "ASIFormDataRequest.h"
#import "Constants.h"
#import "MBProgressHUD.h"
#import "PhotoSource.h"
#import "AlbumPhotoViewController.h"
#import "Database.h"

#define POST_TIP 0
#define POST_RATING_TIP 1
#define POST_SCORE 2

@interface AlertWithInt : TTAlertViewController

@property (nonatomic,assign) NSInteger tipId;

@end

@implementation AlertWithInt

@synthesize tipId;

@end

@implementation DetailViewController

////// StarRatingItemDelegate
- (void)starClicked:(NSInteger)newScore {
    if (! [[Database singleton] setCocktail:_cid score:newScore]) {
        NSLog(@"Problem updating the score of the cocktail");
    }
    NSInteger score = [[Database singleton] getCocktailScore:_cid];
    NSLog(@"score for %d updated to: %d", _cid, score);
    
    // send the score to the website
    NSLog(@"Sending score to cloud: %d", newScore);
    NSString *uniqueIdentifier = [[NSUserDefaults standardUserDefaults] 
                                  stringForKey:@"uid"];
    // Start request
    NSURL *url = [NSURL URLWithString:[BASE_URL stringByAppendingString:@"ratings/post.php"]];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    request.tag = POST_SCORE;
    [request setPostValue:[NSNumber numberWithInt:_cid] 
                   forKey:@"cocktail_id"];
    [request setPostValue:[NSNumber numberWithInt:newScore] forKey:@"score"];
    [request setPostValue:uniqueIdentifier forKey:@"user_id"];
    [request setDelegate:self];
    [request startAsynchronous];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Updating score...";
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// private

- (UIViewController*)post:(NSDictionary*)query {
    
    TTPostController* controller = [[TTPostController alloc] initWithNavigatorURL:nil
                                                                            query:
                                    [NSDictionary dictionaryWithObjectsAndKeys:@"", @"text", nil]];
    controller.originView = [query objectForKey:@"__target__"];
    controller.delegate = self;
    return controller;
}

- (UIViewController*)alertLikeIt:(NSInteger)tipId {
    AlertWithInt* controller = [[AlertWithInt alloc]  initWithNavigatorURL:nil 
                                                                                       query:[NSDictionary dictionaryWithObjectsAndKeys:@"", @"text", nil]];
    // set side data
    controller.tipId = tipId;
    [controller addButtonWithTitle:@"Like it" URL:nil];
    [controller addButtonWithTitle:@"Dislike" URL:nil];
    [controller addCancelButtonWithTitle:@"Cancel" URL:nil];
    controller.delegate = self;
    return controller;
}

- (UIViewController *)albumViewer {
    AlbumPhotoViewController *controller = [[AlbumPhotoViewController alloc] initWithPhotoSource:        [[PhotoSource alloc] initWithTitle:@"Album" andCocktailId:_cid]];
    return controller;
}

/////////////////////////////////////////////////////////////////////////////////////////////////// TTAlertViewControllerDelegate
- (BOOL)alertViewController: (TTAlertViewController*)controller
  didDismissWithButtonIndex: (NSInteger)buttonIndex
                        URL: (NSString*)URL {
    AlertWithInt *ctr = (AlertWithInt *)controller;
    NSInteger dr;
    switch (buttonIndex) {
        case 0: // Like it
            NSLog(@"like it");
            dr = 1;
            break;
        case 1: // Dislike
            NSLog(@"dislike");
            dr = -1;
            break;
        case 2: // Cancel
            NSLog(@"Cancel");
            [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:NO];
            return YES;
        default:
            return NO;
    }
    // Start request
    NSURL *url = [NSURL URLWithString:[BASE_URL stringByAppendingString:@"tips/post_rating.php"]];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    request.tag = POST_RATING_TIP;
    [request setPostValue:[NSNumber numberWithInt:ctr.tipId] 
                   forKey:@"tip_id"];
    [request setPostValue:[NSNumber numberWithInt:dr] forKey:@"delta"];
    [request setDelegate:self];
    [request startAsynchronous];
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Sending rating...";
    return YES;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// TTPostControllerDelegate

- (void)postController: (TTPostController*)postController
           didPostText: (NSString*)text
            withResult: (id)result {
    // doesnt consider empty text
    text = [text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (! text.length) {
        NSString *text = @"The text cannot be empty";
        text = [text stringByReplacingOccurrencesOfString:@" " 
                                               withString:@"%20"
                                                  options:NSRegularExpressionSearch
                                                    range:NSMakeRange(0, text.length)];
        [[TTNavigator navigator] openURLAction:[[TTURLAction actionWithURLPath:[NSString stringWithFormat:@"tt://alert/Error/%@",text]] applyAnimated:YES]];
        return;
    }
    
    // Start request
    NSURL *url = [NSURL URLWithString:[BASE_URL stringByAppendingString:@"tips/post.php"]];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    request.tag = POST_TIP;
    [request setPostValue:[NSNumber numberWithInt:_cid] 
                   forKey:@"cocktail_id"];
    NSString *uniqueIdentifier = [[NSUserDefaults standardUserDefaults] 
                                  stringForKey:@"login"];
    [request setPostValue:uniqueIdentifier forKey:@"user_id"];
    [request setPostValue:text forKey:@"text"];
    [request setPostValue:[NSNumber numberWithDouble:[[NSDate date] timeIntervalSince1970]] 
                forKey:@"date"];
    
    [request setDelegate:self];
    [request startAsynchronous];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Sending comment...";
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// ASI

- (void)requestFinished:(ASIHTTPRequest *)request
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    NSString *responseString = [request responseString];
    NSError *e = nil;
    NSData *data = [responseString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data 
                                                                 options: NSJSONReadingMutableContainers error: &e];
    NSLog(@"Response: %@",responseString);
    NSString *status = [responseDict objectForKey:@"status"];
    if (! [status isEqualToString:@"ok"]) {
        NSLog(@"Error...");
    }
    if (request.tag == POST_TIP || request.tag == POST_SCORE) {
        [self reload];
    }  else if (request.tag == POST_RATING_TIP) {
        NSString *tid = [responseDict objectForKey:@"id"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:[NSString stringWithFormat:@"tip_%@",tid]];
        [self reload];
    }
}

- (void)requestFailed:(ASIHTTPRequest *)request
{    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    NSError *error = [request error];
    NSLog(@"Error: %@", error);
    if (request.tag == POST_TIP) {
        NSLog(@"Error uploading the new comment");
    }
}


///////////////////////////////////////////////////////////////////////////////////////////////////
// NSObject

- (id)initWithCocktailId:(NSInteger)cid {
    if (self = [self initWithCocktailId:cid type:DetailViewControllerTypeStandard]) {
        //
    }
    return self;
}

- (id)initWithCocktailId:(NSInteger)cid type:(DetailViewControllerType)type {
    if (self = [super init]) {
        _cid = cid;
        SingleCocktailDataSource *data;
        if (type == DetailViewControllerTypeStandard)
            data = [[SingleCocktailDataSource alloc] initWithCocktailId:cid];
        else if (type == DetailViewControllerTypeRedMissingIngredients)
            data = [[SingleCocktailFromFridgeDataSource alloc] initWithCocktailId:cid];
        data.starDelegate = self;
        self.dataSource = data;
        self.variableHeightRows = YES;
        self.tableViewStyle = UITableViewStyleGrouped;
        self.hidesBottomBarWhenPushed = YES;
        [[TTNavigator navigator].URLMap from:@"tt://tips/post"
                            toViewController:self 
                                    selector:@selector(post:)];
        [[TTNavigator navigator].URLMap from:@"tt://tips/alert/(alertLikeIt:)"
                            toViewController:self 
                                    selector:@selector(alertLikeIt:)];
        [[TTNavigator navigator].URLMap from:@"tt://albumViewer" 
                            toViewController:self 
                                    selector:@selector(albumViewer)];
    }
    return self;
}

- (void)dealloc {
    [[TTNavigator navigator].URLMap removeURL:@"tt://tips/post"];
    [[TTNavigator navigator].URLMap removeURL:@"tt://tips/alert/(alertLikeIt:)"];
    [[TTNavigator navigator].URLMap removeURL:@"tt://albumViewer"];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// UIViewController

- (void)loadView {
    [super loadView];
    SingleCocktailDataSource *data = self.dataSource;
    self.title = data.cocktail.cocktail.name;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self reload];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// TTModelDelegate

- (void) modelDidFinishLoad:(id< TTModel >)model {
    [super modelDidFinishLoad:model];
    SingleCocktail *cocktail = (SingleCocktail *)model;
    self.title = cocktail.cocktail.name;
}

////////////////////////////////////////////////////////////////////////////////
/// UIImagePickerControllerDelegate, UINavigationControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)img editingInfo:(NSDictionary *)editInfo {
    NSLog(@"imagePickerControllerDidFinishPickingImage");
}

@end
