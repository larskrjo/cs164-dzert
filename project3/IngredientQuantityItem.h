//
//  IngredientQuantityItem.h
//  project3
//
//  Created by Edouard GODFREY on 6/17/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import <Three20/Three20.h>

@interface IngredientQuantityItem : TTTableCaptionItem

@property (nonatomic, assign) NSInteger selected;

+ (id)itemWithText:(NSString *)text caption:(NSString *)caption selected:(NSInteger)selected URL:(NSString *)url;

@end

@interface IngredientQuantityItemCell : TTTableCaptionItemCell

@end  