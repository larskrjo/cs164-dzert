//
//  ImagePickerViewController.m
//  project3
//
//  Created by Edouard GODFREY on 6/6/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import "CocktailAlbumViewController.h"
#import "Constants.h"
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"
#import "PhotoSource.h"
#import <Three20/Three20.h>
#import "MBProgressHUD.h"
#import "GraphicsUtil.h"

#define POST_PHOTO 0

static CGFloat kThumb = 43; 
static CGFloat kSmall = 80; 
//static CGFloat kBig = 320;

@interface CocktailAlbumViewController ()

@property (nonatomic, assign) NSInteger cocktailId;

@end

@implementation CocktailAlbumViewController

@synthesize imgPicker=_imgPicker;
@synthesize cocktailId=_cocktailId;

- (id)initWithChooseOption:(NSInteger)option andCocktailId:(NSInteger)cid {
    self = [super init];
    if (self) {
        self.cocktailId = cid;
        self.photoSource = [[PhotoSource alloc] initWithTitle:@"Album" andCocktailId:_cocktailId];
        self.imgPicker = [[UIImagePickerController alloc] init];
        NSLog(@"initWithChooseOption");
        if (option==PICK_ALBUM)
            self.imgPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        else if (option==PICK_CAMERA)
            self.imgPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        self.imgPicker.allowsEditing = YES;
        self.imgPicker.delegate = self;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self presentModalViewController:self.imgPicker animated:YES];
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)img editingInfo:(NSDictionary *)editInfo {
    [self dismissModalViewControllerAnimated:YES];
    
    // sending image to cloud
    NSLog(@"Sending new image to cloud");
    NSString *uniqueIdentifier = [[NSUserDefaults standardUserDefaults] 
                                  stringForKey:@"uid"];
    // Start request
    NSURL *url = [NSURL URLWithString:[BASE_URL stringByAppendingString:@"photos/post.php"]];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    request.tag = POST_PHOTO;
    [request setPostValue:[NSNumber numberWithInt:_cocktailId] 
                   forKey:@"cocktail_id"];
    [request setPostValue:uniqueIdentifier forKey:@"user_id"];
    NSData *imgData = UIImagePNGRepresentation(img);
    [request setData:imgData forKey:@"photo_big"];
    
    UIImage *thumbImg = [GraphicsUtil imageWithImage:img 
                                        scaledToSize:CGSizeMake(kThumb, kThumb)];
    NSData *thumbData = UIImagePNGRepresentation(thumbImg);
    [request setData:thumbData forKey:@"photo_thumb"];
    
    UIImage *smallImg = [GraphicsUtil imageWithImage:img 
                                        scaledToSize:CGSizeMake(kSmall, kSmall)];
    NSData *smallData = UIImagePNGRepresentation(smallImg);
    [request setData:smallData forKey:@"photo_small"];
    
    [request setDelegate:self];
    [request startAsynchronous];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Uploading picture...";

}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    NSString *responseString = [request responseString];
    NSLog(@"Response: %@", responseString);
    if (request.tag == POST_PHOTO) {
        NSLog(@"Succes uploading the picture");
        NSError *e = [[NSError alloc] init ];
        NSData *data = [responseString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data 
                                                                     options: NSJSONReadingMutableContainers error: &e];
        NSString *status = [responseDict objectForKey:@"status"];
        if ([status isEqualToString:@"ok"]) {
            NSString *path = [responseDict objectForKey:@"status"];
            [(PhotoSource *)self.photoSource addPhotoPath:path];
            [self reload];
        } //else error
        
    }
}

- (void)requestFailed:(ASIHTTPRequest *)request
{    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    NSError *error = [request error];
    NSLog(@"Error: %@", error);
    if (request.tag == POST_PHOTO) {
        NSLog(@"Error uploading the picture");
    }
}


@end
