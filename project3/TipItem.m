//
//  TableItemTextWithRating.m
//  project3
//
//  Created by Edouard GODFREY on 6/7/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import <Three20/Three20.h>
#import "TipItem.h"
#import "Tip.h"
#import "Three20/Three20+Additions.h"

@implementation TipItem

@synthesize ratingPlus=_ratingPlus, ratingMinus=_ratingMinus, voteEnabled=_voteEnabled;

///////////////////////////////////////////////////////////////////////////////////////////////////  
// class public  

+ (id)itemWithTitle:(NSString*)title
               text:(NSString*)text 
          timestamp:(NSDate*)date
                URL:(NSString*)url
         ratingPlus:(NSInteger)plus
        ratingMinus:(NSInteger) minus 
        voteEnabled:(BOOL)enabled
{
    TipItem *item = [[TipItem alloc] init];
    item.title = title;
    item.text = text;
    item.URL = url;
    item.timestamp = date;
    item.ratingMinus = minus;
    item.ratingPlus = plus;
    item.voteEnabled = enabled;
    return item;  
}  

///////////////////////////////////////////////////////////////////////////////////////////////////  
// NSObject  

- (id)init {  
    if (self = [super init]) {  
        _ratingPlus = 0;
        _ratingMinus = 0;
    }  
    return self;  
} 

@end

///////////////////////////////////////////////////////////////////////////////////////////////////  

static CGFloat kHPadding = 10; 
static CGFloat kVPadding = 10; 
static CGFloat kRatingHeight = 5;
static CGFloat kRatingWidth = 50;
static CGFloat maxWidth = 280;

///////////////////////////////////////////////////////////////////////////////////////////////////  

//////////////////////////////////////////////////////////////  
//////   TableTextWithRatingItemCell     ////  
//////////////////////////////////////////////////////////////  

@implementation TipItemCell  

@synthesize green=_green;
@synthesize red=_red;
@synthesize lblRating=_lblRating;

+ (CGFloat)tableView:(UITableView*)tableView rowHeightForObject:(id)item {  
    TipItem* textItem = item;
    UIFont *font = TTSTYLEVAR(font);
    CGSize title = [textItem.title sizeWithFont:font  
                              constrainedToSize:CGSizeMake(maxWidth, CGFLOAT_MAX)  
                                  lineBreakMode:UILineBreakModeWordWrap];
    CGSize textSize = [textItem.text sizeWithFont:font  
                                constrainedToSize:CGSizeMake(maxWidth, CGFLOAT_MAX)  
                                    lineBreakMode:UILineBreakModeWordWrap];
    
    return kVPadding*6 + textSize.height + title.height + kVPadding;  
}

///////////////////////////////////////////////////////////////////////////////////////////////////  

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)identifier { 
    if (self = [super initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:identifier]) {  
        _item = nil;
        self.red = [[UIView alloc] init];
        self.red.height = kRatingHeight;
        self.green = [[UIView alloc] init];
        self.green.height = kRatingHeight;
        self.lblRating = [[UILabel alloc] init];
        self.lblRating.backgroundColor = [UIColor clearColor];
        self.lblRating.font = self.timestampLabel.font;
        [self.contentView addSubview:self.green];
        [self.contentView addSubview:self.red];
        [self.contentView addSubview:self.lblRating];
        self.detailTextLabel.numberOfLines = 0;
    }  
    return self;  
}

///////////////////////////////////////////////////////////////////////////////////////////////////  
// UIView  

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    [super setHighlighted:highlighted animated:animated];
    if (highlighted) {
        _lblRating.textColor = [UIColor whiteColor];
        _green.backgroundColor = [UIColor whiteColor];
        _red.backgroundColor = [UIColor lightGrayColor];
    } else {
        _lblRating.textColor = TTSTYLEVAR(timestampTextColor);
        _green.backgroundColor = [UIColor greenColor];
        _red.backgroundColor = [UIColor redColor];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    if (_selectable) {
    if (selected) {
        _lblRating.textColor = [UIColor whiteColor];
        _green.backgroundColor = [UIColor whiteColor];
        _red.backgroundColor = [UIColor lightGrayColor];
    } else {
        _lblRating.textColor = TTSTYLEVAR(timestampTextColor);
        _green.backgroundColor = [UIColor greenColor];
        _red.backgroundColor = [UIColor redColor];
    }
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self.timestampLabel.superview bringSubviewToFront:self.timestampLabel];
    self.detailTextLabel.size = [self.detailTextLabel.text sizeWithFont:TTSTYLEVAR(font)  
                                                      constrainedToSize:CGSizeMake(maxWidth, CGFLOAT_MAX)  
                                                          lineBreakMode:UILineBreakModeWordWrap];
    self.detailTextLabel.top += kVPadding;
    self.timestampLabel.left = 230;
    self.lblRating.right = self.timestampLabel.right;
    self.lblRating.top = self.detailTextLabel.bottom + 2*kVPadding;
    self.red.right = self.lblRating.left-kHPadding;
    self.red.top = (self.lblRating.top + self.lblRating.bottom) / 2.0 - self.red.size.height / 2.0;
    self.green.right = self.red.left;
    self.green.top = self.red.top;
}  

///////////////////////////////////////////////////////////////////////////////////////////////////  
// TTTableViewCell  

- (id)object {  
    return _item;  
}  

- (void)setObject:(id)object {  
    if (_item != object) {  
        [super setObject:object];  
        self.selectionStyle = TTSTYLEVAR(tableSelectionStyle);
        
        TipItem* item = object;  
        
        _lblRating.text = [NSString stringWithFormat:@"[%d/%d like it]",item.ratingPlus,item.ratingPlus+item.ratingMinus];
        [_lblRating sizeToFit];
        
        if (item.ratingPlus) {
            self.green.hidden = NO;
            self.green.width = kRatingWidth * 
            (item.ratingPlus / ((float) item.ratingPlus+item.ratingMinus ));
        } else {
            self.green.hidden = YES;
        }
        if (item.ratingMinus) {
            self.red.hidden = NO;
            self.red.width = kRatingWidth *
            (item.ratingMinus / ((float) item.ratingPlus+item.ratingMinus ));
        } else {
            self.red.hidden = YES;
        }
        if (item.voteEnabled) {
            self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            _selectable = YES;
        } else {
            self.accessoryType = UITableViewCellAccessoryNone;
            self.selectionStyle = UITableViewCellSelectionStyleNone;
            _selectable = NO;
        }
    }  
}

@end