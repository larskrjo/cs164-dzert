//
//  IngredientDataSource.m
//  project3
//
//  Created by Edouard GODFREY on 6/15/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import "IngredientDataSource.h"
#import "Database.h"
#import "Ingredient.h"
#import "Three20/Three20+Additions.h"
#import "IngredientItem.h"

///////////////////////////////////////////////////////////////////////////////////////////////////

@implementation IngredientBook

@synthesize ingredients = _ingredients;
@synthesize allIngredients=_allIngredients;

// public
- (void)switchIngredientId:(NSInteger)iid {
    for (Ingredient *i in _ingredients) {
        if (i.uid == iid) {
            NSLog(@"switching %@ with id %d", i.name, i.uid);
            i.selected = 1-i.selected;
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// private

- (void)loadFromDB {
    //if (! self.allIngredients) {
    NSMutableArray *ingredients;
    if (_fromFridge) {
        ingredients = [[Database singleton] selectedIngredients];
    } else {
        ingredients = [[Database singleton] ingredients];
    }
    self.allIngredients = ingredients;
    self.ingredients = ingredients;
    //}
    _loadedDB = YES;
    for (id delegate in _delegates) {
        [delegate performSelectorOnMainThread:@selector(modelDidFinishLoad:) withObject:self waitUntilDone:NO];
    }
}

// public
- (id)initFromFridge:(BOOL)fromFridge {
    if (self = [super init]) {
        _delegates = nil;
        _ingredients = nil;
        _allIngredients = nil;
        _loadingStarted = NO;
        _loadedDB = NO;
        _fromFridge = fromFridge;
    }
    return self;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// TTModel

- (NSMutableArray*)delegates {
    if (!_delegates) {
        _delegates = TTCreateNonRetainingArray();
    }
    return _delegates;
}

- (BOOL)isLoadingMore {
    return NO;
}

- (BOOL)isOutdated {
    return NO;
}

- (BOOL)isLoaded {
    return _loadedDB;
}


- (BOOL)isLoading {
    return _loadingStarted && ![self isLoaded];
}

- (BOOL)isEmpty {
    return !_ingredients.count;
}

- (void)load:(TTURLRequestCachePolicy)cachePolicy more:(BOOL)more {
    [_delegates perform:@selector(modelDidStartLoad:) withObject:self];
    _loadingStarted = YES;
    NSOperationQueue *q = [[NSOperationQueue alloc] init];
    [q addOperationWithBlock:^{
        [self loadFromDB];
    }];
}

- (void)invalidate:(BOOL)erase {
}

- (void)cancel {
    [_delegates perform:@selector(modelDidCancelLoad:) withObject:self];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// public

- (void)search:(NSString*)text {
    [self cancel];
    
    if (text.length) {
        [_delegates perform:@selector(modelDidStartLoad:) withObject:self];
        _loadingStarted = YES;
        NSOperationQueue *q = [[NSOperationQueue alloc] init];
        [q addOperationWithBlock: ^{
            // effective search    
            self.ingredients = [[NSMutableArray alloc] init];
            for (Ingredient *i in _allIngredients) {
                NSRange range = [i.name rangeOfString:text 
                                              options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)];
                if (range.length > 0) {
                    [self.ingredients addObject:i];
                }
            }
            _loadingStarted = NO;
            for (id delegate in _delegates) {
                [delegate performSelectorOnMainThread:@selector(modelDidFinishLoad:) withObject:self waitUntilDone:NO];
            }
        }];
    } else {
        [_delegates perform:@selector(modelDidChange:) withObject:self];
    }
}

@end

///////////////////////////////////////////////////////////////////////////////////////////////////

@implementation IngredientDataSource

@synthesize ingredientBook = _ingredientBook;

/// public
-(id)init {
    if (self = [super init]) {
        _ingredientBook = [[IngredientBook alloc] initFromFridge:NO];
        self.model = _ingredientBook;
    }
    return self;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// UITableViewDataSource

- (NSArray*)sectionIndexTitlesForTableView:(UITableView*)tableView {
    return [TTTableViewDataSource lettersForSectionsWithSearch:YES summary:NO];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// TTTableViewDataSource

- (Class)tableView:(UITableView*)tableView cellClassForObject:(id) object {  
    if ([object isKindOfClass:[IngredientItem class]]) {
        return [IngredientItemCell class];
    } else {  
        return [super tableView:tableView cellClassForObject:object];  
    }  
}

- (void)tableViewDidLoadModel:(UITableView*)tableView {
    self.items = [NSMutableArray array];
    self.sections = [NSMutableArray array];
    
    NSMutableDictionary* groups = [NSMutableDictionary dictionary];
    for (Ingredient* i in _ingredientBook.ingredients) {
        NSString *name = i.name;
        char firstLetter = [name characterAtIndex:0];
        if (firstLetter < 'A' || firstLetter > 'Z')
            firstLetter = '#';
        NSString* letter = [NSString stringWithFormat:@"%C", firstLetter];
        NSMutableArray* section = [groups objectForKey:letter];
        if (!section) {
            section = [NSMutableArray array];
            [groups setObject:section forKey:letter];
        }
        [section addObject:[IngredientItem itemWithIngredient:i]];
    }
    
    NSArray* letters = [groups.allKeys sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
    for (NSString* letter in letters) {
        NSArray* items = [groups objectForKey:letter];
        [_sections addObject:letter];
        [_items addObject:items];
    }
}

- (id<TTModel>)model {
    return _ingredientBook;
}

@end

///////////////////////////////////////////////////////////////////////////////////////////////////

@implementation IngredientSearchDataSource

@synthesize ingredientBook = _ingredientBook;

///////////////////////////////////////////////////////////////////////////////////////////////////
// NSObject

- (id)init {
    if (self = [super init]) {
        _ingredientBook = [[IngredientBook alloc] initFromFridge:NO];
        self.model = _ingredientBook;
    }
    return self;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// TTTableViewDataSource

- (Class)tableView:(UITableView*)tableView cellClassForObject:(id) object { 
    if ([object isKindOfClass:[IngredientItem class]]) {
        return [IngredientItemCell class];
    } else {  
        return [super tableView:tableView cellClassForObject:object];  
    }  
}

- (void)tableViewDidLoadModel:(UITableView*)tableView {
    self.items = [[NSMutableArray alloc] init];
    for (Ingredient* i in _ingredientBook.ingredients) {
        [_items addObject:
         [IngredientItem itemWithIngredient:i]];
    }
}

- (void)search:(NSString*)text {
    [_ingredientBook search:text];
}

- (id<TTModel>)model {
    return _ingredientBook;
}

- (NSString*)titleForLoading:(BOOL)reloading {
    return @"Searching...";
}

- (NSString*)titleForEmpty {
    return @"No ingredients found";
}

@end

@implementation IngredientFridgeDataSource
@synthesize swipeDeleteDelegate=_swipeDeleteDelegate;

- (id)init {
    if (self = [super init]) {
        self.ingredientBook = [[IngredientBook alloc] initFromFridge:YES];
        self.model = self.ingredientBook;
    }
    return self;
}

- (NSString*)titleForEmpty {
    return @"Click on + to add ingredients";
}

- (void)tableViewDidLoadModel:(UITableView*)tableView {
    self.items = [[NSMutableArray alloc] init];
    for (Ingredient* i in self.ingredientBook.ingredients) {
        [_items addObject:[IngredientItem itemWithIngredient:i]];
    }
    if (self.ingredientBook.ingredients.count > 0)
        [_items addObject:[TTTableButton 
                           itemWithText:@"What can I make ?"
                           URL:@"tt://fridge/available"]];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle 
forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [_swipeDeleteDelegate tableView:tableView swipeDeleteAtIndexPath:indexPath];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == [tableView numberOfRowsInSection:0]-1)
        return NO;
    return YES;
}


@end