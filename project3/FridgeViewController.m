//
//  FridgeViewController.m
//  project3
//
//  Created by Edouard GODFREY on 6/16/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import "FridgeViewController.h"
#import "IngredientDataSource.h"
#import "Three20/Three20+Additions.h"
#import "IngredientItem.h"
#import "Database.h"

@implementation FridgeViewController
@synthesize done=_done, edit=_edit;
///////////////////////////////////////////////////////////////////////////////////////////////////
// NSObject

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        self.title = @"Fridge";
        IngredientFridgeDataSource *dataSource = [[IngredientFridgeDataSource alloc] init];
        dataSource.swipeDeleteDelegate = self;
        self.dataSource = dataSource;
        self.done = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(switchEdit)];
        self.edit = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(switchEdit)];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    UIBarButtonItem *plus = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:@"tt://fridge/ingredients" action:@selector(openURLFromButton:)];
    self.navigationController.navigationBar.topItem.rightBarButtonItem = plus;
}

- (void)switchEdit {
    self.tableView.editing = ! self.tableView.editing;
    if (self.tableView.editing) {
        self.navigationController.navigationBar.topItem.leftBarButtonItem = _done;
    } else {
        self.navigationController.navigationBar.topItem.leftBarButtonItem = _edit;
    }
}

- (void)modelDidFinishLoad:(id<TTModel>)model {
    [super modelDidFinishLoad:model];
    if ([self.tableView numberOfRowsInSection:0] > 0)
        if (self.tableView.editing) {
            self.navigationController.navigationBar.topItem.leftBarButtonItem = _done;
        } else {
            self.navigationController.navigationBar.topItem.leftBarButtonItem = _edit;
        }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self reload];
}

- (void)         tableView:(UITableView *)tableView 
    swipeDeleteAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"swipeDeleted");
    IngredientItemCell *cell = (IngredientItemCell*) [tableView cellForRowAtIndexPath:indexPath];
    IngredientItem *item = (IngredientItem*) [cell object];
    NSOperationQueue *q = [[NSOperationQueue alloc] init];
    [q addOperationWithBlock:^{
        [[Database singleton] setSelected:0 forIngredientId:item.ingredient.uid];
        [self performSelectorOnMainThread:@selector(reload) withObject:self waitUntilDone:NO];
    }];
}


@end