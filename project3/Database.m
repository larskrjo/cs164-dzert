//
//  Database.m
//  project3
//
//  Created by Edouard Godfrey and Lars Kristian Johansen on 4/10/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import "Database.h"
#import "Cocktail.h"
#import "Ingredient.h"
#import "Constants.h"

@implementation Database

static Database *singleton = nil;

+ (Database *)singleton {
    if (singleton == nil) {
        singleton = [[super alloc] init];
    }
    return singleton;
}

- (id)init {
    if (self = [super init]) {
        // connect to the DB
        NSString *path = [[NSBundle mainBundle] pathForResource:@"database" ofType:@"db"];
        sqlite3_open([path UTF8String], &db);
    }
    return self;
}

- (NSMutableArray *) cocktails {
    @synchronized(self) {
        NSMutableArray *ret = [[NSMutableArray alloc] initWithCapacity:6300];
        //NSString *sql = @"SELECT name FROM cocktail ORDER BY name ASC";
        NSString *sql = @"SELECT cocktail.id, cocktail.name, GROUP_CONCAT(ingredient.name,', ') "
        "FROM cocktail "
        "JOIN cocktail_component ON cocktail.id = cocktail_component.cocktail_id  "
        "JOIN component ON cocktail_component.component_id = component.id  "
        "JOIN ingredient ON component.ingredient_id = ingredient.id  "
        "GROUP BY cocktail.id  "
        "ORDER BY cocktail.name ASC ;";
        sqlite3_stmt *stmt;
        sqlite3_prepare_v2(db, [sql UTF8String], -1, &stmt, nil);
        while (sqlite3_step(stmt) == SQLITE_ROW) {
            int current_id = sqlite3_column_int(stmt, 0);
            const char *current_name = (char *) sqlite3_column_text(stmt, 1);
            const char *current_description = (char *) sqlite3_column_text(stmt, 2);
            [ret addObject:[Cocktail 
                            cocktailWithID:current_id
                            andName:[NSString stringWithUTF8String:current_name]
                            andDescription:[NSString stringWithUTF8String:current_description]]];
        }
        return ret;
    }
}

- (NSMutableArray *) ingredients {
    @synchronized(self) {
        NSMutableArray *ret = [[NSMutableArray alloc] init];
        NSString *sql = @"SELECT id, name, selected FROM ingredient ORDER BY name ASC";
        sqlite3_stmt *stmt;
        sqlite3_prepare_v2(db, [sql UTF8String], -1, &stmt, nil);
        while (sqlite3_step(stmt) == SQLITE_ROW) {
            int uid = sqlite3_column_int(stmt, 0);
            const char *current_name = (char *) sqlite3_column_text(stmt, 1);
            NSString *name = [NSString stringWithUTF8String:current_name];
            int selected = sqlite3_column_int(stmt, 2);
            [ret addObject:[Ingredient ingredientWithID:uid andName:name selected:selected]];
        }
        return ret;
    }
}

- (NSMutableArray *) selectedIngredients {
    @synchronized(self) {
        NSMutableArray *ret = [[NSMutableArray alloc] init];
        NSString *sql = @"SELECT id, name FROM ingredient WHERE selected=1 ORDER BY name ASC";
        sqlite3_stmt *stmt;
        sqlite3_prepare_v2(db, [sql UTF8String], -1, &stmt, nil);
        while (sqlite3_step(stmt) == SQLITE_ROW) {
            int uid = sqlite3_column_int(stmt, 0);
            const char *current_name = (char *) sqlite3_column_text(stmt, 1);
            NSString *name = [NSString stringWithUTF8String:current_name];
            int selected = 0;
            [ret addObject:[Ingredient ingredientWithID:uid andName:name selected:selected]];
        }
        return ret;
    }
}

- (NSMutableArray *) cocktailsFromFridge {
    @synchronized(self) {
        NSMutableArray *ret = [[NSMutableArray alloc] init];
        
        // query to find the percentage covered by the ingredients for each cocktail
        NSString *sql = [NSString stringWithFormat:
               @"SELECT cocktail.id, cocktail.name, avg(ingredient.selected), "
               "GROUP_CONCAT(ingredient.name,', ') "
               "FROM cocktail "
               "JOIN cocktail_component ON cocktail.id = cocktail_component.cocktail_id  "
               "JOIN component ON cocktail_component.component_id = component.id  "
               "JOIN ingredient ON component.ingredient_id = ingredient.id  "
               "GROUP BY cocktail.id  "
               "ORDER BY avg(ingredient.selected) DESC "
               "LIMIT %d;", MAX_LIST_SIZE];
        sqlite3_stmt *stmt;
        if (sqlite3_prepare_v2(db, [sql UTF8String], -1, &stmt, nil) != SQLITE_OK)
            NSLog(@"Error cocktailsByIngredient: %@", [NSString stringWithUTF8String:(const char*)sqlite3_errmsg(db)]);
        while (sqlite3_step(stmt) == SQLITE_ROW) {
            int cid = sqlite3_column_int(stmt, 0);
            const char *name_ = (char *) sqlite3_column_text(stmt, 1);
            NSString *name = [NSString stringWithUTF8String:name_];
            double score = sqlite3_column_double(stmt, 2);
            const char *description_ = (char *) sqlite3_column_text(stmt, 3);
            NSString *description = [NSString stringWithUTF8String:description_];
            [ret addObject:[Cocktail cocktailWithID:cid andName:name andDescription:description andScore:score]];
            NSLog(@"Cocktail found: %@; score=%f", name, score);
        }
        NSLog(@"Total number found: %d", ret.count);
        
        return ret;
    }
}
/*
- (NSString *)buildSelectQuery:(NSInteger)selected withIngredient:(NSMutableSet *)ingredients {
    NSString *sql = [NSString 
                     stringWithFormat:@"UPDATE ingredient SET selected = %d WHERE ", selected];
    NSInteger i = 0;
    for (NSString *name in ingredients) {
        if (i == ingredients.count - 1) {
            sql = [sql stringByAppendingFormat:@"name=\"%@\";", name];
        } else {
            sql = [sql stringByAppendingFormat:@"name=\"%@\" OR ", name];
        }
        sqlite3_exec(db, [sql UTF8String], 0, nil, nil);
        i++;
    }
    
    return sql;
}*/

- (NSString*) recipeByCocktailId: (NSInteger)cid {
    @synchronized(self) {
        NSString *sql = [NSString stringWithFormat: @"SELECT recipe FROM cocktail WHERE id=%d", cid];
        sqlite3_stmt *stmt;
        sqlite3_prepare_v2(db, [sql UTF8String], -1, &stmt, nil);
        NSString *result;
        while (sqlite3_step(stmt) == SQLITE_ROW) {
            const char *current_name = (char *) sqlite3_column_text(stmt, 0);
            result = [NSString stringWithUTF8String:current_name];
        }
        return result;
    }
}

- (NSMutableArray*) ingredientByCocktailId: (NSInteger)cid {
    @synchronized(self) {
        NSMutableArray *array = [[NSMutableArray alloc] init];
        NSString *sql = [NSString stringWithFormat: @"SELECT ingredient.id, ingredient.name, ingredient.selected FROM cocktail, cocktail_component, component, ingredient, quantity WHERE cocktail.id = cocktail_component.cocktail_id AND cocktail_component.component_id = component.id AND component.quantity_id = quantity.id and component.ingredient_id = ingredient.id AND cocktail.id=%d", cid];
        sqlite3_stmt *stmt;
        sqlite3_prepare_v2(db, [sql UTF8String], -1, &stmt, nil);
        while (sqlite3_step(stmt) == SQLITE_ROW) {
            int uid = sqlite3_column_int(stmt, 0);
            const char *name_ = (char *) sqlite3_column_text(stmt, 1);
            NSString *name = [NSString stringWithUTF8String:name_];
            int selected = sqlite3_column_int(stmt, 2);
            [array addObject:[Ingredient ingredientWithID:uid andName:name selected:selected]]; 
        }
        return array;
    }
}

- (NSMutableArray*) ingredientQuantitiesByCocktailId: (NSInteger)cid {
    @synchronized(self) {
        NSMutableArray *array = [[NSMutableArray alloc] init];
        NSString *sql = [NSString stringWithFormat: @"SELECT quantity.name FROM cocktail, cocktail_component, component, ingredient, quantity WHERE cocktail.id = cocktail_component.cocktail_id AND cocktail_component.component_id = component.id AND component.quantity_id = quantity.id and component.ingredient_id = ingredient.id AND cocktail.id=%d", cid];
        sqlite3_stmt *stmt;
        sqlite3_prepare_v2(db, [sql UTF8String], -1, &stmt, nil);
        while (sqlite3_step(stmt) == SQLITE_ROW) {
            const char *name = (char *) sqlite3_column_text(stmt, 0);
            NSString *s = [NSString stringWithUTF8String:name];
            [array addObject:[s stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
        }
        return array;
    }
}

- (BOOL)setCocktail:(NSInteger)cid score:(NSInteger)newScore { 
    @synchronized(self) {
        NSInteger old_score_c;
        NSString *sql = [NSString stringWithFormat:@"SELECT score FROM cocktail WHERE id=%d",cid ];
        sqlite3_stmt *stmt;
        sqlite3_prepare_v2(db, [sql UTF8String], -1, &stmt, nil);
        if (sqlite3_step(stmt) == SQLITE_ROW)
            old_score_c = sqlite3_column_int(stmt, 0);
        else
            return NO;
        
        // update cocktail
        sql = [NSString stringWithFormat:@"UPDATE cocktail SET score=%d WHERE id=%d",
               newScore, cid];
        sqlite3_exec(db, [sql UTF8String], 0, nil, nil);
        
        // update ingredient
        NSInteger score_incr = newScore - old_score_c;
        sql = [NSString stringWithFormat:
               @"UPDATE ingredient SET score=score+%d, score_count=score_count+1 WHERE "
               "id IN "
               "(SELECT component.ingredient_id "
               "FROM cocktail "
               "JOIN cocktail_component ON cocktail.id = cocktail_component.cocktail_id "
               "JOIN component ON cocktail_component.component_id = component.id "
               "WHERE cocktail.id=%d)", score_incr, cid];
        NSLog(@"sql: %@",sql);
        if (sqlite3_exec(db, [sql UTF8String], 0, nil, nil) != SQLITE_OK) {
            NSLog(@"error UPDATE ingredient: %@", 
                  [NSString stringWithUTF8String:(const char*)sqlite3_errmsg(db)]);
            return NO;
        }
        return YES;
    }
}

- (BOOL)setSelected:(NSInteger)selected forIngredientId:(NSInteger)iid { 
    @synchronized(self) {
        
        // update cocktail
        NSString *sql = [NSString stringWithFormat:@"UPDATE ingredient SET selected=%d WHERE id=%d",
               selected, iid];
        if (sqlite3_exec(db, [sql UTF8String], 0, nil, nil)) {
            return YES;
        } else {
            return NO;
        }
    }
}


- (NSInteger)getCocktailScore:(NSInteger)cid {
    @synchronized(self) {
        NSString *sql = [NSString 
                         stringWithFormat:@"SELECT score FROM cocktail WHERE id=%d",
                         cid ];
        sqlite3_stmt *stmt;
        sqlite3_prepare_v2(db, [sql UTF8String], -1, &stmt, nil);
        if (sqlite3_step(stmt) == SQLITE_ROW) {
            return sqlite3_column_int(stmt, 0);
        }
        return 0;
    }
}

- (NSInteger)cocktailIdFromName:(NSString *)name {
    @synchronized(self) {
        NSString *sql = [NSString 
                         stringWithFormat:@"SELECT id FROM cocktail WHERE name=\"%@\"",
                         name ];
        sqlite3_stmt *stmt;
        sqlite3_prepare_v2(db, [sql UTF8String], -1, &stmt, nil);
        if (sqlite3_step(stmt) == SQLITE_ROW) {
            return sqlite3_column_int(stmt, 0);
        }
        return -1; 
    }
}

- (Cocktail*)cocktailFromId:(NSInteger)cocktail_id {
    @synchronized(self) {
        NSString *sql = [NSString 
                         stringWithFormat:
                         @"SELECT cocktail.id, cocktail.name, GROUP_CONCAT(ingredient.name,', ') "
                         "FROM cocktail "
                         "JOIN cocktail_component ON cocktail.id = cocktail_component.cocktail_id  "
                         "JOIN component ON cocktail_component.component_id = component.id  "
                         "JOIN ingredient ON component.ingredient_id = ingredient.id  "
                         "WHERE cocktail.id=%d",
                         cocktail_id ];
        sqlite3_stmt *stmt;
        sqlite3_prepare_v2(db, [sql UTF8String], -1, &stmt, nil);
        if (sqlite3_step(stmt) == SQLITE_ROW) {
            int current_id = sqlite3_column_int(stmt, 0);
            const char *current_name = (char *) sqlite3_column_text(stmt, 1);
            const char *current_description = (char *) sqlite3_column_text(stmt, 2);
            return [Cocktail cocktailWithID:current_id
                                    andName:[NSString stringWithUTF8String:current_name]
                             andDescription:[NSString stringWithUTF8String:current_description]];
        }
        return nil; 
    }
}

- (NSString*)cocktailNameFromID:(NSInteger)cocktail_id {
    @synchronized(self) {
        NSString *sql = [NSString 
                         stringWithFormat:@"SELECT name FROM cocktail WHERE id=%d",
                         cocktail_id ];
        sqlite3_stmt *stmt;
        sqlite3_prepare_v2(db, [sql UTF8String], -1, &stmt, nil);
        if (sqlite3_step(stmt) == SQLITE_ROW) {
            const char *name = (char *) sqlite3_column_text(stmt, 0);
            return [NSString stringWithUTF8String:name];
        }
        return nil; 
    }
}

- (NSMutableArray *)cocktailsJustForYou {
    @synchronized(self) {
        NSMutableArray *ret = [[NSMutableArray alloc] initWithCapacity:MAX_LIST_SIZE];
        NSString *sql = [NSString 
                         stringWithFormat:
                        @"SELECT cocktail.id, cocktail.name, GROUP_CONCAT(ingredient.name,', '), avg(ingredient.score / (1.0*ingredient.score_count)) "
                         "FROM cocktail "
                         "JOIN cocktail_component ON cocktail.id = cocktail_component.cocktail_id  "
                         "JOIN component ON cocktail_component.component_id = component.id  "
                         "JOIN ingredient ON component.ingredient_id = ingredient.id  "
                         "WHERE cocktail.score = 0 "
                         "GROUP BY cocktail.id  "
                         "ORDER BY avg(ingredient.score / 1.0*ingredient.score_count) DESC "
                         "LIMIT %d;", MAX_LIST_SIZE ];
        NSLog(@"query: %@", sql);
        sqlite3_stmt *stmt;
        if (sqlite3_prepare_v2(db, [sql UTF8String], -1, &stmt, nil) != SQLITE_OK)
            NSLog(@"Error topUnratedCocktails: %@", [NSString stringWithUTF8String:(const char*)sqlite3_errmsg(db)]);
        while (sqlite3_step(stmt) == SQLITE_ROW) {
            int current_id = sqlite3_column_int(stmt, 0);
            const char *current_name = (char *) sqlite3_column_text(stmt, 1);
            const char *current_description = (char *) sqlite3_column_text(stmt, 2);
            double score = sqlite3_column_double(stmt, 3);
            [ret addObject:[Cocktail 
                            cocktailWithID:current_id
                            andName:[NSString stringWithUTF8String:current_name]
                            andDescription:[NSString stringWithUTF8String:current_description]
                            andScore:score]];
        }
        NSLog(@"Total number found: %d", ret.count);
        return ret;
    }
}

- (NSMutableArray *)cocktailsAlreadyRated {
    @synchronized(self) {
        NSMutableArray *ret = [[NSMutableArray alloc] initWithCapacity:MAX_LIST_SIZE];
        NSString *sql = [NSString stringWithFormat:
        @"SELECT cocktail.id, cocktail.name, GROUP_CONCAT(ingredient.name,', '), cocktail.score "
        "FROM cocktail "
        "JOIN cocktail_component ON cocktail.id = cocktail_component.cocktail_id  "
        "JOIN component ON cocktail_component.component_id = component.id  "
        "JOIN ingredient ON component.ingredient_id = ingredient.id  "
        "WHERE cocktail.score > 0 "
        "GROUP BY cocktail.id  "
        "ORDER BY cocktail.score DESC "
        "LIMIT %d;", MAX_LIST_SIZE];
        NSLog(@"query: %@", sql);
        sqlite3_stmt *stmt;
        if (sqlite3_prepare_v2(db, [sql UTF8String], -1, &stmt, nil) != SQLITE_OK)
            NSLog(@"Error topRatedCocktails: %@", [NSString stringWithUTF8String:(const char*)sqlite3_errmsg(db)]);
        while (sqlite3_step(stmt) == SQLITE_ROW) {
            int current_id = sqlite3_column_int(stmt, 0);
            const char *current_name = (char *) sqlite3_column_text(stmt, 1);
            const char *current_description = (char *) sqlite3_column_text(stmt, 2);
            double score = sqlite3_column_double(stmt, 3);
            [ret addObject:[Cocktail 
                            cocktailWithID:current_id
                            andName:[NSString stringWithUTF8String:current_name]
                            andDescription:[NSString stringWithUTF8String:current_description]
                            andScore:score]];
        }
        NSLog(@"Total number found: %d", ret.count);
        return ret;
    }
}

@end
