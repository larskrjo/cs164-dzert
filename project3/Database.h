//
//  Database.h
//  project3
//
//  Created by Edouard Godfrey and Lars Kristian Johansen on 4/10/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "/usr/include/sqlite3.h"
#import "Cocktail.h"

/**
 * @interface This interface implements all operations needed to interact
 *            the database
 */
@interface Database : NSObject {
    sqlite3 *db;
}

/**
 * @brief returns the singleton Database
 */
+ (Database *)singleton;

/**
 * @brief returns all cocktails as product in lexicographical order
 * of their name
 */
- (NSMutableArray *) cocktails;

/**
 * @brief returns all ingredients name in lexicographical order
 */
- (NSMutableArray *) ingredients;
/**
 * @brief returns all ingredients name in lexicographical order
 */
- (NSMutableArray *) selectedIngredients;

/**
 * @brief returns the cocktail recipe for the given cocktail name
 */
- (NSString*) recipeByCocktailId: (NSInteger)cid;

/**
 * @brief returns the ingredient for the given cocktail id
 */
- (NSMutableArray*) ingredientByCocktailId: (NSInteger)cid;

/**
 * @brief returns the ingredient quantites for the given cocktail name
 */
- (NSMutableArray*) ingredientQuantitiesByCocktailId: (NSInteger)cid;

- (BOOL)setSelected:(NSInteger)selected forIngredientId:(NSInteger)iid;

/**
 * @brief returns the cocktails that can be made from the given ingredients 
 */
- (NSMutableArray *) cocktailsFromFridge;

/**
 * @brief set a new score for a cocktail c whose name is @a name
 *
 * @post new_score(c)=newScore
 * @post for all ingredient i contained by c:
 *       if (old_score(i) == NULL)
 *          new_score_count(i)=old_score_count(i)+1
 *          new_score(i)=old_score(i)+newScore
 *       else
 *          new_score_count(i)=old_score_count(i)
 *          new_score(i)=old_score(i)+newScore-old_score(c)
 * @return true iff all updates in the db were sucessful
 * 
 */
- (BOOL)setCocktail:(NSInteger)cid score:(NSInteger)newScore;

/**
 * @brief Gives the cocktail score for the given name.
 * @return the database score of the cocktail whose name is @a name
 * @return 0 if no score where set yet
 */
- (NSInteger)getCocktailScore:(NSInteger)cid;

/**
 * @brief Gives the top @n unrated cocktail scores.
 * @return result: an array of size @a n of the top unrated 
 *         CocktailScore inferred from the scores already 
 *         given by the user in descending order.
 */
- (NSMutableArray *)cocktailsJustForYou;

/**
 * @brief Get the top @a n cocktail scores.
 * @return result: an array of size @a n of the top already rated 
 *         CocktailScore in descending order.
 */
- (NSMutableArray *)cocktailsAlreadyRated;

/**
 * @brief Gives the cocktail id by the name provided.
 * @return the database id of the cocktail whose name is @a name
 * @return -1 if no such name
 */
- (NSInteger)cocktailIdFromName:(NSString *)name;

/**
 * @brief Gives the cocktail name by the ID provided.
 * @return the database name of the cocktail whose id is @a id
 * @return nil if no such id
 */
- (Cocktail*)cocktailFromId:(NSInteger)cocktail_id;
- (NSString*)cocktailNameFromID:(NSInteger)cocktail_id;


@end