//
//  AppDelegate.h
//  project3
//
//  Created by Edouard Godfrey and Lars Kristian Johansen on 4/10/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TabBarController.h"

/**
 * @interface The application that launches the application.
 */
@interface AppDelegate : NSObject <UIApplicationDelegate>

@end
