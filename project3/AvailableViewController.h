//
//  AvailableViewController.h
//  project3
//
//  Created by Edouard GODFREY on 6/16/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import <Three20/Three20.h>

@class CocktailDataSource;

@interface AvailableViewController : TTTableViewController <TTSearchTextFieldDelegate>

@end