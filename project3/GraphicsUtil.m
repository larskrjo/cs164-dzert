//
//  GraphicsUtil.m
//  project3
//
//  Created by Edouard Godfrey and Lars Kristian Johansen on 5/2/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import "GraphicsUtil.h"

@implementation GraphicsUtil

+ (UIView *)composeImageWithImage1:(UIImage *)im1 
                         andImage2:(UIImage *)im2
               withProportionOfIm1:(double)proportion
                    andTotalNumber:(NSInteger)total {
    UIView *result = [[UIView alloc] 
                      initWithFrame:CGRectMake(0, 0, 
                                               im1.size.width*total, 
                                               im2.size.height)];
    NSInteger threshold = proportion;
    double modulo = proportion - threshold;
    for (int i = 0; i < total; i++) {
        UIImageView *sub;
        if (i != threshold) {
            if (i < threshold)
                sub = [[UIImageView alloc] initWithImage:im1];
            else
                sub = [[UIImageView alloc] initWithImage:im2];
            sub.frame = CGRectMake(i*im1.size.width,0,im1.size.width,im1.size.height);
            [result addSubview:sub];
        } else {
            // combine void_star & star
            CGRect rect1 = CGRectMake(0, 0, im1.size.width*modulo, im1.size.height);
            CGRect rect2 = CGRectMake(im1.size.width*modulo, 0, im1.size.width*(1-modulo), im1.size.height);
            if (modulo > 0) {
                UIImage *part1 = [GraphicsUtil crop:im1 withRect:rect1];
                sub = [[UIImageView alloc] initWithImage:part1];
                sub.frame = CGRectMake(i*im1.size.width,0,rect1.size.width,rect1.size.height);
                [result addSubview:sub];
            }
            UIImage *part2 = [GraphicsUtil crop:im2 withRect:rect2];
            UIImageView *sub2 = [[UIImageView alloc] initWithImage:part2];
            sub2.frame = CGRectMake(i*im1.size.width+rect1.size.width,0,rect2.size.width,rect2.size.height);
            [result addSubview:sub2];
        }
    }
    return result;
    
}

+ (UIImage *)composeBisImageWithImage1:(UIImage *)im1 
                         andImage2:(UIImage *)im2
               withProportionOfIm1:(double)proportion
                    andTotalNumber:(NSInteger)total {
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(im1.size.width * total, im1.size.height), NO, 0.0); 
    NSInteger threshold = proportion;
    double modulo = proportion - threshold;
    float x = 0.0;
    for (int i = 0; i < total; i++) {
        if (i != threshold) {
            UIImage *img = (i < threshold) ? im1 : im2;
            [img drawAtPoint:CGPointMake(x, 0)];
            x += img.size.width;
        } else {
            // combine void_star & star
            CGRect rect1 = CGRectMake(0, 0, im1.size.width*modulo, im1.size.height);
            CGRect rect2 = CGRectMake(im1.size.width*modulo, 0, im1.size.width*(1-modulo), im1.size.height);
            if (modulo > 0) {
                UIImage *part1 = [GraphicsUtil crop:im1 withRect:rect1];
                [part1 drawAtPoint:CGPointMake(x, 0)];
                x += part1.size.width;
            }
            UIImage *part2 = [GraphicsUtil crop:im2 withRect:rect2];
            [part2 drawAtPoint:CGPointMake(x, 0)];
            x += part2.size.width;
        }
    }
    UIImage *answer = UIGraphicsGetImageFromCurrentImageContext(); 
    UIGraphicsEndImageContext();
    return answer;
    
}

+ (UIImage*)crop:(UIImage *)imageToCrop withRect:(CGRect)rect
{
    //create a context to do our clipping in
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef currentContext = UIGraphicsGetCurrentContext();
    
    //create a rect with the size we want to crop the image to
    //the X and Y here are zero so we start at the beginning of our
    //newly created context
    CGRect clippedRect = CGRectMake(0, 0, rect.size.width, rect.size.height);
    CGContextClipToRect( currentContext, clippedRect);
    
    //create a rect equivalent to the full size of the image
    //offset the rect by the X and Y we want to start the crop
    //from in order to cut off anything before them
    CGRect drawRect = CGRectMake(rect.origin.x * -1,
                                 rect.origin.y * -1,
                                 imageToCrop.size.width,
                                 imageToCrop.size.height);
    
    //draw the image to our clipped context using our offset rect
    CGContextTranslateCTM(currentContext, 0.0, drawRect.size.height);
    CGContextScaleCTM(currentContext, 1.0, -1.0);
    
    CGContextDrawImage(currentContext, drawRect, imageToCrop.CGImage);
    
    //pull the image from our cropped context
    UIImage *cropped = UIGraphicsGetImageFromCurrentImageContext();
    
    //pop the context to get back to the default
    UIGraphicsEndImageContext();
    
    //Note: this is autoreleased
    return cropped;
}

+ (UIImage*)imageWithImage:(UIImage*)image 
              scaledToSize:(CGSize)newSize;
{
    UIGraphicsBeginImageContext( newSize );
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

@end
