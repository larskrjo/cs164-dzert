//
//  SuggestionViewController.h
//  project3
//
//  Created by Edouard Godfrey and Lars Kristian Johansen on 4/10/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//
#import <Three20/Three20.h>
#import "CocktailDataSource.h"

@interface SuggestionViewController : TTTableViewController <TTTabDelegate>

@property (nonatomic,strong) TTTabBar *tab;
@property (nonatomic,strong) CocktailListDataSource *alreadyRated;
@property (nonatomic,strong) CocktailListDataSource *justForYou;
@property (nonatomic,strong) CocktailListDataSource *byPopularity;

@end