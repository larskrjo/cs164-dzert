//
//  BrowseDataSource.h
//  project3
//
//  Created by Edouard GODFREY on 6/5/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import <Three20/Three20.h>
#import "CocktailWithScoreItem.h"
#import "DetailViewController.h"

typedef enum {
    CocktailBookTypeAll,                   // every cocktails in db
    CocktailBookTypeFromFridge,            // cocktails doable from fridge content
    CocktailBookTypeAlreadyRated,
    CocktailBookTypeByPopularity,
    CocktailBookTypeJustForYou
} CocktailBookType;

/*
 * a searchable model which can be configured with a 
 * loading and/or search time
 */
@interface CocktailBook : NSObject <TTModel> {
    NSMutableArray* _delegates;
    NSMutableArray* _cocktails;
    NSArray* _allCocktails;
    BOOL _loadingStarted;
    BOOL _loadedDB;
    BOOL _loadedInternet;
    CocktailBookType _type;
}

@property(nonatomic,strong) NSMutableArray* cocktails;
@property(nonatomic,copy) NSArray* allCocktails;
@property(nonatomic,strong) NSSet* cocktailsWithPhotos;

- (id)init;
- (id)initWithType:(CocktailBookType)type;

- (void)search:(NSString*)text;

@end

@interface CocktailDataSource : TTSectionedDataSource

@property(nonatomic,strong,readonly) CocktailBook* cocktailBook;

@end

@interface CocktailSearchDataSource : TTSectionedDataSource

@property(nonatomic,readonly) CocktailBook* cocktailBook;

- (id)init;

@end

@interface CocktailListDataSource : TTSectionedDataSource {
    CocktailWithScoreItemType _type;
    DetailViewControllerType _dvType;
}

@property(nonatomic,strong,readonly) CocktailBook* cocktailBook;

- (id)initWithCocktailBookType:(CocktailBookType)bookType type:(CocktailWithScoreItemType)type detailViewControllerType:(DetailViewControllerType)dvType;

@end