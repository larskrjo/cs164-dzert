//
//  AppDelegate.m
//  project3
//
//  Created by Edouard Godfrey and Lars Kristian Johansen on 4/10/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
// 

#import "AppDelegate.h"
#import <UIKit/UIKit.h>
#import "BrowseViewController.h"
#import "IngredientViewController.h"
#import "SuggestionViewController.h"
#import "TabBarController.h"
#import "FridgeViewController.h"
#import "AvailableViewController.h"
#import "ChoosePhotoAlertViewController.h"
#import "CocktailAlbumViewController.h"
#import <Three20/Three20.h>
#import <Three20UI/UIViewAdditions.h>
#import "StyleSheet.h"


@implementation AppDelegate

- (void)applicationDidFinishLaunching:(UIApplication *)application
{
    
    [TTStyleSheet setGlobalStyleSheet:[[StyleSheet alloc] init]];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (! [defaults stringForKey:@"uid"]) {
        // defaults not set
        [defaults setObject:[self uuid] forKey:@"uid"];
        NSLog(@"New uid: %@", [defaults stringForKey:@"uid"]);
    }
    
    [[TTURLRequestQueue mainQueue] setMaxContentLength:0];
    
    TTNavigator* navigator = [TTNavigator navigator];
    navigator.persistenceMode = TTNavigatorPersistenceModeAll;
    navigator.window = [[UIWindow alloc] initWithFrame:TTScreenBounds()];
    
    TTURLMap* map = navigator.URLMap;
    
    // Any URL that doesn't match will fall back on this one, and open in the web browser
    [map from:@"*" toViewController:[TTWebController class]];

    [map                from:@"tt://alert/(alertWithTitle:)/(message:)" 
            toViewController:self 
                    selector:@selector(alertWithTitle:message:)];
    
    // The tab bar controller is shared, meaning there will only ever be one created.  Loading
    // This URL will make the existing tab bar controller appear if it was not visible.
    [map from:@"tt://tabBar" toSharedViewController:[TabBarController class]];
    
    [map from:@"tt://cocktails" toSharedViewController:[BrowseViewController class]];
    [map from:@"tt://fridge" toSharedViewController:[FridgeViewController class]];
    [map from:@"tt://fridge/ingredients" toViewController:[IngredientViewController class]];
    [map from:@"tt://fridge/available" toViewController:[AvailableViewController class]];
    [map from:@"tt://suggestions" toSharedViewController:[SuggestionViewController class]];
    
    // By specifying the parent URL, we are saying that the tab containing menu #5 will be
    // selected before opening this URL, ensuring that about controllers are only pushed
    // inside the tab containing the about menu
    [map from:@"tt://cocktails/(initWithCocktailId:)" 
        toViewController:[DetailViewController class]];
    [map from:@"tt://cocktails/(initWithCocktailId:)/(type:)"
        toViewController:[DetailViewController class]];
  
    [map from:@"tt://photos/alertPicker/(initWithCocktailId:)" toViewController:[ChoosePhotoAlertViewController class]];
    [map from:@"tt://photos/picker/(initWithChooseOption:)/(andCocktailId:)" toViewController:[CocktailAlbumViewController class]];
    
    // Before opening the tab bar, we see if the controller history was persisted the last time
    if (![navigator restoreViewControllers]) {
        // This is the first launch, so we just start with the tab bar
        [navigator openURLAction:[TTURLAction actionWithURLPath:@"tt://tabBar"]];
    }
}

- (UIViewController*)alertWithTitle:(NSString *)title message:(NSString *)message {
    TTAlertViewController* alert = [[TTAlertViewController alloc]
                                     initWithTitle:title
                                     message:message];
    [alert addCancelButtonWithTitle:@"OK" URL:nil];
    return alert;
}

- (NSString *)uuid
{
    CFUUIDRef uuidRef = CFUUIDCreate(NULL);
    CFStringRef uuidStringRef = CFUUIDCreateString(NULL, uuidRef);
    CFRelease(uuidRef);
    return (__bridge_transfer NSString *)uuidStringRef;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

/*
// Optional UITabBarControllerDelegate method.
- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
}
*/

/*
// Optional UITabBarControllerDelegate method.
- (void)tabBarController:(UITabBarController *)tabBarController didEndCustomizingViewControllers:(NSArray *)viewControllers changed:(BOOL)changed
{
}
*/

@end
