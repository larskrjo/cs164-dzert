//
//  IngredientQuantityItem.m
//  project3
//
//  Created by Edouard GODFREY on 6/17/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import "IngredientQuantityItem.h"
#import <Three20/Three20.h>

@implementation IngredientQuantityItem
@synthesize selected=_selected;

+ (id)itemWithText:(NSString *)text caption:(NSString *)caption selected:(NSInteger)selected URL:(NSString *)url {
    IngredientQuantityItem *item = [[IngredientQuantityItem alloc] init];
    if (item) {
        item.text = text;
        item.caption = caption;
        item.selected = selected;
        item.URL = url;
    }
    return item;
}

@end

@implementation IngredientQuantityItemCell

- (void)setObject:(id)object {
    [super setObject:object];
    IngredientQuantityItem *item = object;
    if (item.selected == 0) {
        self.detailTextLabel.textColor = [UIColor redColor];
        self.detailTextLabel.text = [self.detailTextLabel.text stringByAppendingString:@" [missing]"];
    } else {
        self.detailTextLabel.textColor = [UIColor blackColor];
    }
}

@end


