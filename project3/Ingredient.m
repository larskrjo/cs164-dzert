//
//  Ingredient.m
//  project3
//
//  Created by Edouard GODFREY on 6/15/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import "Ingredient.h"

@implementation Ingredient
@synthesize uid=_uid, name=_name, selected=_selected;

+ (id)ingredientWithID:(NSInteger)uid andName:(NSString *)name selected:(NSInteger)selected {
    Ingredient *ing = [[Ingredient alloc] init];
    if (ing) {
        ing.name = name;
        ing.uid = uid;
        ing.selected = selected;
    }
    return ing;
}

@end
