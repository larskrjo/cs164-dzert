//
//  BrowseDataSource.m
//  project3
//
//  Created by Edouard GODFREY on 6/5/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import "CocktailDataSource.h"
#import "Database.h"
#import "Cocktail.h"
#import "Three20/Three20+Additions.h"
#import "Constants.h"
#import "ASIFormDataRequest.h"
#import "CocktailWithScoreItem.h"
#import "DetailViewController.h"

#define GET_COCKTAILS_WITH_PHOTOS 0
#define MOST_POPULAR_COCKTAILS 1

@interface TTTableSubtitleItemCellFixed : TTTableSubtitleItemCell
@end
@implementation TTTableSubtitleItemCellFixed
-(void)prepareForReuse {
    [super prepareForReuse];
    _imageView2 = nil;
}
@end

///////////////////////////////////////////////////////////////////////////////////////////////////

@implementation CocktailBook

@synthesize cocktails = _cocktails;
@synthesize allCocktails=_allCocktails;
@synthesize cocktailsWithPhotos=_cocktailsWithPhotos;

///////////////////////////////////////////////////////////////////////////////////////////////////
// private

- (void)loadFromDBAndInternet {
    //if (! self.allCocktails) {
        NSMutableArray *cocktails;
        switch(_type) {
            case CocktailBookTypeAll:
            {
                cocktails = [[Database singleton] cocktails];
                // query the internet for cocktails with photos
                NSURL *url = [NSURL URLWithString:[BASE_URL stringByAppendingString:@"photos/cocktailsWithPhoto.php"] ];
                ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
                request.tag = GET_COCKTAILS_WITH_PHOTOS;
                [request setDelegate:self];
                [request startAsynchronous];
            }
                break;
            case CocktailBookTypeFromFridge:
                cocktails = [[Database singleton] cocktailsFromFridge];
                // no need to load from internet
                _loadedInternet = YES;
                break;
            case CocktailBookTypeJustForYou:
                cocktails = [[Database singleton] cocktailsJustForYou];
                // no need to load from internet
                _loadedInternet = YES;
                break;
            case CocktailBookTypeAlreadyRated:
                cocktails = [[Database singleton] cocktailsAlreadyRated];
                // no need to load from internet
                _loadedInternet = YES;
                break;
            case CocktailBookTypeByPopularity:
            {
                // Start request
                NSURL *url = [NSURL URLWithString:[BASE_URL stringByAppendingString:@"ratings/most_popular.php"] ];
                ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
                request.tag = MOST_POPULAR_COCKTAILS;
                [request setPostValue:[NSNumber numberWithInt:MAX_LIST_SIZE] 
                               forKey:@"number" ];
                [request setDelegate:self];
                [request startAsynchronous]; 
            }
                break;
        }
        self.allCocktails = cocktails;
        self.cocktails = cocktails;
    //}
    _loadedDB = YES;
    if ([self isLoaded]) {
        for (id delegate in _delegates) {
            [delegate performSelectorOnMainThread:@selector(modelDidFinishLoad:) withObject:self waitUntilDone:NO];
        }
    }
}
///////////////////////////////////////////////////////////////////////////////////////////////////
// ASI

- (void)requestFinished:(ASIHTTPRequest *)request
{
    NSString *responseString = [request responseString];
    NSLog(@"Response: %@", responseString);
    NSError *e = nil;
    NSString *status;
    NSData *data = [responseString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data 
                                                                 options: NSJSONReadingMutableContainers error: &e];
    switch (request.tag) {
        case GET_COCKTAILS_WITH_PHOTOS:
        {
            status = [responseDict objectForKey:@"status"];
            if ([status isEqualToString:@"ok"]) {
                self.cocktailsWithPhotos = [NSSet setWithArray:[responseDict objectForKey:@"id"]];
            }
            _loadedInternet = YES;
        }
            break;
        case MOST_POPULAR_COCKTAILS:
        {
            NSArray *result = [responseDict objectForKey:@"list"];
            NSMutableArray *cocktails = [NSMutableArray array];
            for (NSDictionary *cocktail in result) {
                NSLog(@"id:__ %d", [[cocktail objectForKey:@"cocktail_id"] intValue]);
                NSNumber *cocktailId = [cocktail objectForKey:@"cocktail_id"];
                Cocktail *c = [[Database singleton] 
                                  cocktailFromId:[cocktailId intValue]];
                c.score = [[cocktail objectForKey:@"score"] doubleValue];
                NSLog(@"Cloud: %d (%@) has a score of %f", c.uid,
                      c.name, c.score);
                [cocktails addObject:c];
            }
            self.allCocktails = cocktails;
            self.cocktails = cocktails;
            _loadedInternet = YES;
        }
            break;
    }
    if ([self isLoaded]) {
        for (id delegate in _delegates) {
            [delegate performSelectorOnMainThread:@selector(modelDidFinishLoad:) withObject:self waitUntilDone:NO];
        }
    }
}

- (void)requestFailed:(ASIHTTPRequest *)request
{    
    NSError *error = [request error];
    NSLog(@"Error: %@", error);
    switch (request.tag) {
        case GET_COCKTAILS_WITH_PHOTOS:
            NSLog(@"Couldn't get list of cocktails with photos");
            _loadedInternet = YES;
            break;
        case MOST_POPULAR_COCKTAILS:
            NSLog(@"Couldn't get list of cocktails by popularity");
            _loadedInternet = YES;
            break;
    }
    if ([self isLoaded]) {
        for (id delegate in _delegates) {
            [delegate performSelectorOnMainThread:@selector(modelDidFinishLoad:) withObject:self waitUntilDone:NO];
        }
    }
}


///////////////////////////////////////////////////////////////////////////////////////////////////
// NSObject

- (id)init {
    if (self = [super init]) {
        _delegates = nil;
        _cocktails = nil;
        _allCocktails = nil;
        _loadingStarted = NO;
        _loadedInternet = NO;
        _loadedDB = NO;
        _type = CocktailBookTypeAll;
    }
    return self;
}

- (id)initWithType:(CocktailBookType)type {
    if (self = [super init]) {
        _delegates = nil;
        _cocktails = nil;
        _allCocktails = nil;
        _loadingStarted = NO;
        _type = type;
    }
    return self;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// TTModel

- (NSMutableArray*)delegates {
    if (!_delegates) {
        _delegates = TTCreateNonRetainingArray();
    }
    return _delegates;
}

- (BOOL)isLoadingMore {
    return NO;
}

- (BOOL)isOutdated {
    return NO;
}

- (BOOL)isLoaded {
    return _loadedDB && _loadedInternet;
}


- (BOOL)isLoading {
    return _loadingStarted && ![self isLoaded];
}

- (BOOL)isEmpty {
    NSLog(@"isEmpty %d", !_cocktails.count);
    return !_cocktails.count;
}

- (void)load:(TTURLRequestCachePolicy)cachePolicy more:(BOOL)more {
    _loadedInternet = NO;
    _loadedDB = NO;
    NSLog(@"load:cachePolicy:more--");
    [_delegates perform:@selector(modelDidStartLoad:) withObject:self];
    _loadingStarted = YES;
    NSOperationQueue *q = [[NSOperationQueue alloc] init];
    [q addOperationWithBlock:^{
        [self loadFromDBAndInternet];
    }];
}

- (void)invalidate:(BOOL)erase {
}

- (void)cancel {
    [_delegates perform:@selector(modelDidCancelLoad:) withObject:self];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// public

- (void)search:(NSString*)text {
    [self cancel];
    
    if (text.length) {
        [_delegates perform:@selector(modelDidStartLoad:) withObject:self];
        _loadingStarted = YES;
        NSOperationQueue *q = [[NSOperationQueue alloc] init];
        [q addOperationWithBlock: ^{
            // effective search    
            self.cocktails = [[NSMutableArray alloc] init];
            for (Cocktail *c in _allCocktails) {
                NSRange range = [c.name rangeOfString:text 
                                              options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)];
                if (range.length > 0) {
                    [self.cocktails addObject:c];
                }
            }
            _loadingStarted = NO;
            for (id delegate in _delegates) {
                [delegate performSelectorOnMainThread:@selector(modelDidFinishLoad:) withObject:self waitUntilDone:NO];
            }
        }];
    } else {
        [_delegates perform:@selector(modelDidChange:) withObject:self];
    }
}

@end

///////////////////////////////////////////////////////////////////////////////////////////////////

@implementation CocktailDataSource

@synthesize cocktailBook = _cocktailBook;

///////////////////////////////////////////////////////////////////////////////////////////////////
// NSObject

- (id)init {
    if (self = [super init]) {
        _cocktailBook = [[CocktailBook alloc] init];
        self.model = _cocktailBook;
    }
    return self;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// UITableViewDataSource

- (NSArray*)sectionIndexTitlesForTableView:(UITableView*)tableView {
    return [TTTableViewDataSource lettersForSectionsWithSearch:YES summary:NO];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// TTTableViewDataSource

- (Class)tableView:(UITableView*)tableView cellClassForObject:(id) object {  
    if ([object isKindOfClass:[TTTableSubtitleItem class]]) {
        return [TTTableSubtitleItemCellFixed class];
    } else {  
        return [super tableView:tableView cellClassForObject:object];  
    }  
}

- (void)tableViewDidLoadModel:(UITableView*)tableView {
    self.items = [NSMutableArray array];
    self.sections = [NSMutableArray array];
    
    NSMutableDictionary* groups = [NSMutableDictionary dictionary];
    for (Cocktail* c in _cocktailBook.cocktails) {
        NSString *name = c.name;
        char firstLetter = [name characterAtIndex:0];
        if (firstLetter < 'A' || firstLetter > 'Z')
            firstLetter = '#';
        NSString* letter = [NSString stringWithFormat:@"%C", firstLetter];
        NSMutableArray* section = [groups objectForKey:letter];
        if (!section) {
            section = [NSMutableArray array];
            [groups setObject:section forKey:letter];
        }
        NSString *url = 
            ([_cocktailBook.cocktailsWithPhotos 
              containsObject:[NSNumber numberWithInt:c.uid]]) ? 
            [BASE_URL stringByAppendingFormat:@"photos/selected/%d.png",c.uid] 
            : nil;
        TTTableItem *item = 
            [TTTableSubtitleItem 
                                 itemWithText:name 
                                     subtitle:c.description 
                                     imageURL:url
                                 defaultImage:nil
                                          URL:[NSString stringWithFormat:@"tt://cocktails/%d",c.uid]
                                 accessoryURL:nil];
        [section addObject:item];
    }
    
    NSArray* letters = [groups.allKeys sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
    for (NSString* letter in letters) {
        NSArray* items = [groups objectForKey:letter];
        [_sections addObject:letter];
        [_items addObject:items];
    }
}

- (id<TTModel>)model {
    return _cocktailBook;
}

@end

///////////////////////////////////////////////////////////////////////////////////////////////////

@implementation CocktailSearchDataSource

@synthesize cocktailBook = _cocktailBook;

///////////////////////////////////////////////////////////////////////////////////////////////////
// NSObject

- (id)init {
    if (self = [super init]) {
        _cocktailBook = [[CocktailBook alloc] init];
        self.model = _cocktailBook;
    }
    return self;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// TTTableViewDataSource

- (Class)tableView:(UITableView*)tableView cellClassForObject:(id) object {  
    if ([object isKindOfClass:[TTTableSubtitleItem class]]) { 
        return [TTTableSubtitleItemCellFixed class];
    } else {  
        return [super tableView:tableView cellClassForObject:object];  
    }  
}

- (void)tableViewDidLoadModel:(UITableView*)tableView {
    NSLog(@"tableViewDidLoadModel:tableView(search)");
    self.items = [[NSMutableArray alloc] init];
    for (Cocktail* c in _cocktailBook.cocktails) {
        NSString *name = c.name;
        NSString *url = 
        ([_cocktailBook.cocktailsWithPhotos 
          containsObject:[NSNumber numberWithInt:c.uid]]) ? 
        [BASE_URL stringByAppendingFormat:@"photos/selected/%d.png",c.uid] 
        : nil;
        TTTableItem *item =
        [TTTableSubtitleItem itemWithText:name 
                                 subtitle:c.description 
                                 imageURL:url
                             defaultImage:nil
                                      URL:[NSString stringWithFormat:@"tt://cocktails/%d",c.uid] 
                             accessoryURL:nil];
        [_items addObject:item];
    }
    if (! self.items.count)
        NSLog(@"items are empty");
}

- (void)search:(NSString*)text {
    [_cocktailBook search:text];
}

- (NSString*)titleForLoading:(BOOL)reloading {
    return @"Searching...";
}

- (NSString*)titleForNoData {
    return @"No cocktails found";
}

@end

@implementation CocktailListDataSource
@synthesize cocktailBook=_cocktailBook;

- (id)initWithCocktailBookType:(CocktailBookType)bookType type:(CocktailWithScoreItemType)type detailViewControllerType:(DetailViewControllerType)dvType {
    if (self = [super init]) {
        _cocktailBook = [[CocktailBook alloc] initWithType:bookType];
        self.model = _cocktailBook;
        _type = type;
        _dvType = dvType;
    }
    return self;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// TTTableViewDataSource
- (Class)tableView:(UITableView*)tableView cellClassForObject:(id) object {  
    if ([object isKindOfClass:[CocktailWithScoreItem class]]) { 
        return [CocktailWithScoreItemCell class];
    } else {  
        return [super tableView:tableView cellClassForObject:object];  
    }  
}

- (void)tableViewDidLoadModel:(UITableView*)tableView {
    self.items = [[NSMutableArray alloc] init];
    for (Cocktail* c in _cocktailBook.cocktails) {
        TTTableItem *item =
        [CocktailWithScoreItem itemWithText:c.name subtitle:c.description URL:[NSString stringWithFormat:@"tt://cocktails/%d/%d",c.uid,_dvType] score:c.score type:_type];
        [_items addObject:item];
    }
}

- (NSString*)titleForLoading:(BOOL)reloading {
    return @"Loading...";
}

- (NSString*)titleForEmpty {
    return @"No cocktails found";
}

@end
